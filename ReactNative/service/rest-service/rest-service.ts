import axios, { AxiosInstance } from 'axios';
import Env from '../../enviroment';

const axiosInstance = axios.create({
    baseURL: Env.apiUrl
})

export default axiosInstance;

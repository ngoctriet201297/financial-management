import * as Print from 'expo-print';
import { Order } from '../../models/order/order';
import { UserLoginInformation } from '../../utility/UserLoginInformation';
import moment from 'moment';
import { FormatNumber } from '../../utility/format-numbet';
import { OrderDetail } from '../../models/order/order-detail';

export class PrinterService {

    static setTemplate1(posOrder: Order): string {
        return `<div style="width: 400px; text-align: center; display: block">
        <div style="width: auto;
        margin: auto;
        font-family: tahoma;
        max-width: 400px;
        padding: 10px !important;">
            <div style="
              background-image: linear-gradient(
                to right,
                #818181 50%,
                rgba(255, 255, 255, 0) 0%
              );
              background-position: bottom;
              background-size: 10px 1px;
              background-repeat: repeat-x;
              padding-bottom: 0.5rem !important;
              padding-top: 0.25rem !important;
              margin-top: 0.5rem !important;
              text-align: center !important;
            ">
                <h6 style="
                font-family: tahoma;
                font-size: 0.9375rem;
                margin-bottom: 0.25rem;
                font-weight: bold;
              ">
                    NAK COFFEE
                </h6>
                <p style="
                font-family: Tahoma;
                font-size: 0.75rem;
                margin-bottom: 0.125rem;
              ">
                    Địa chỉ Nak Coffee
                </p>
                <p style="
                font-family: Tahoma;
                font-size: 0.75rem;
                margin-bottom: 0.125rem;
              ">
                    Điện thoại: 0987654321
                </p>
            </div>
            <div style="
              background-image: linear-gradient(
                to right,
                #818181 50%,
                rgba(255, 255, 255, 0) 0%
              );
              background-position: bottom;
              background-size: 10px 1px;
              background-repeat: repeat-x;
              text-align: left !important;
              padding-bottom: 0.5rem !important;
              padding-top: 0.25rem !important;
              margin-top: 0.5rem !important;
            ">
                <div style="
                text-align: center !important;
                margin-bottom: 0.5rem !important;
              ">
                    <h6 style="
                  font-family: tahoma;
                  font-size: 0.9375rem;
                  margin-bottom: 0.25rem;
                ">
                        HÓA ĐƠN THANH TOÁN
                    </h6>
                </div>
                <p style="font-size: 0.6875rem; margin-bottom: 0.125rem;">
                    Số hóa đơn: # ${posOrder?.orderNumber}
                </p>
                <p style="font-size: 0.6875rem; margin-bottom: 0.125rem;">
                    NV: ${UserLoginInformation.fullname}
                </p>
                <p style="font-size: 0.6875rem; margin-bottom: 0.125rem;">
                    Ngày: ${moment(posOrder?.orderDate).format('dd/MM/yyyy HH:mm')}
                </p>
                <p style="font-size: 0.6875rem; margin-bottom: 0.125rem;">
                    Bàn/Khách: ${posOrder?.customersName}
                </p>
                <p style="font-size: 0.6875rem; margin-bottom: 0.125rem;">
                    Loại đơn hàng: ${posOrder?.orderTypeName}
                </p>
            </div>
            <div style="
              background-image: linear-gradient(
                to right,
                #818181 50%,
                rgba(255, 255, 255, 0) 0%
              );
              background-position: bottom;
              background-size: 10px 1px;
              background-repeat: repeat-x;
              padding-top: 0.25rem !important;
              margin-top: 0.5rem !important;
            ">
                <div style="
                color: #000000;
                font-size: 0.625rem;
                width: 100%;
                margin-bottom: 1rem;
              ">
                    <div style="display: flex !important;">
                        <div style="width: 7%; margin-right: 0.6875rem;">
                            <p style="margin-bottom: 0.5rem;">STT</p>
                        </div>
                        <div style="width: 34%; margin-right: 0.9375rem;">
                            <p style="margin-bottom: 0.5rem;">Tên sản phẩm</p>
                        </div>
                        <div style="width: 4.5%; margin-right: 0.9375rem;">
                            <p style="margin-bottom: 0.5rem;">SL</p>
                        </div>
                        <div style="width: 13.5%; margin-right: 1.3125rem;">
                            <p style="margin-bottom: 0.5rem;">Đơn giá</p>
                        </div>
                        <div style="width: 18.5%;">
                            <p style="margin-bottom: 0.5rem;">Thành tiền</p>
                        </div>
                    </div>
                    ${PrinterService.setItem1(posOrder?.orderDetails || [])}
                </div>
            </div>
            <div style="
              font-size: 0.8125rem;
              padding-top: 0.25rem !important;
              margin-top: 0.5rem !important;
            ">
                <div style="text-align: left !important; float: left !important;">
                    <b>Tổng tiền:</b>
                    <p style="margin-bottom: 0.1875rem;">Khuyến mãi:</p>
                    <b style="margin-bottom: 0.1875rem;">Tổng thanh toán:</b>
                </div>
                <div style="text-align: right !important; float: right !important;">
                    <b>${FormatNumber.currency(posOrder?.totalPrice || 0)}</b>
                    <p style="margin-bottom: 0.1875rem;">-${FormatNumber.currency(posOrder?.totalDiscount || 0)}</p>
                    <b style="margin-bottom: 0.1875rem;">${FormatNumber.currency(posOrder?.totalAmount || 0)}</b>
                </div>
                <div style="
                clear: both;
                padding-bottom: 1.5rem !important;
                padding-top: 1.5rem !important;
                text-align: center !important;
              ">
                    <p style="margin-bottom: 1.25rem;">
                        Quý khách cần lấy hóa đơn tài chính vui lòng thông báo khi
                        mua hàng.
                    </p>
                    <p style="margin-bottom: 1.25rem;">Cảm ơn & hẹn gặp lại!</p>
                </div>
            </div>
        </div>
    </div>`
    }
    private static setItem1(items: OrderDetail[] = []): string {
        let itemsHtml = '';
        items?.forEach((item, i) => {
            itemsHtml +=`
        <div style="display: flex !important;">
            <div style="width: 7%; margin-right: 0.6875rem;">
                <p style="margin-bottom: 0.5rem;">${i + 1}</p>
            </div>
            <div style="width: 34%; margin-right: 0.9375rem;">
                <p style="margin-bottom: 0.5rem;">${item.menuPriceName || '' }</p>
            </div>
            <div style="
      width: 4.5%;
      margin-right: 0.9375rem;
      text-align: center !important;
    ">
                <p style="margin-bottom: 0.5rem;">${item.itemQuantity || 0}</p>
            </div>
            <div style="
      width: 13.5%;
      margin-right: 1.3125rem;
      text-align: right !important;
    ">
                <p style="margin-bottom: 0.5rem;">${item.menuAmountSalling || 0}</p>
            </div>
            <div style="width: 18.5%; text-align: right !important;">
                <p style="margin-bottom: 0.5rem;">${item.itemTotalAmount || 0}</p>
            </div>
        </div>
        <div *ngIf="item.promotionId" style="margin-top: -0.5rem; display: flex !important;">
            <div style="width: 7%; margin-right: 0.6875rem;">
                <p></p>
            </div>
            <div style="
      width: 65%;
      color: #929292;
      margin-right: 0.9375rem;
      text-align: left !important;
    ">
                <p>${item.promotionName || ''}</p>
            </div>
            <div style="width: 18.5%; text-align: right !important;">
                <p>${FormatNumber.currency(item.itemDiscountTotal || 0) }</p>
            </div>
        </div>
        <div *ngIf="item.orderDetailNote" style="
    margin-top: -0.5rem;
    padding-bottom: 0.75rem;
    display: flex !important;
  ">
            <div style="width: 7%; margin-right: 0.6875rem;">
                <p></p>
            </div>
            <div style="
      width: 93%;
      color: #929292;
      margin-right: 0.9375rem;
      text-align: left !important;
    ">
                <i>${item.orderDetailNote  || ''}</i>
            </div>
        </div>
        `});
        return itemsHtml || '';
    }
    static printPanel1(posOrder: Order): Promise<Print.FilePrintResult> {
       return Print.printToFileAsync({
           html: PrinterService.setTemplate1(posOrder)
       })
    }
    static printPanel2(posOrder: Order): Promise<Print.FilePrintResult> {
        return Print.printToFileAsync({
            html: PrinterService.setTemplate2(posOrder)
        })
     }
    static setTemplate2(posOrder: Order): string {
        return `
        <div id="BillPrintPanel2" style="width: 400px; text-align: center;>
            <div style="width: auto;margin: auto;
            font-family: tahoma;
            max-width: 400px;
            padding: 10px !important;">
                <div style="
                  background-image: linear-gradient(
                    to right,
                    #818181 50%,
                    rgba(255, 255, 255, 0) 0%
                  );
                  background-position: bottom;
                  background-size: 10px 1px;
                  background-repeat: repeat-x;
                  padding-bottom: 0.5rem !important;
                  padding-top: 0.25rem !important;
                  margin-top: 0.5rem !important;
                  text-align: center !important;
                ">
                    <h6 style="
                    font-family: tahoma;
                    font-size: 0.9375rem;
                    margin-bottom: 0.25rem;
                    font-weight: bold;
                  ">
                        NAK COFFEE
                    </h6>
                </div>
                <div style="
                  background-image: linear-gradient(
                    to right,
                    #818181 50%,
                    rgba(255, 255, 255, 0) 0%
                  );
                  background-position: bottom;
                  background-size: 10px 1px;
                  background-repeat: repeat-x;
                  text-align: left !important;
                  padding-bottom: 0.5rem !important;
                  padding-top: 0.25rem !important;
                  margin-top: 0.5rem !important;
                ">
                    <div style="
                    text-align: center !important;
                    margin-bottom: 0.5rem !important;
                  ">
                        <h6 style="
                      font-family: tahoma;
                      font-size: 0.9375rem;
                      margin-bottom: 0.25rem;
                    ">
                            ĐƠN HÀNG
                        </h6>
                    </div>
                    <p style="font-size: 0.6875rem; margin-bottom: 0.125rem;">
                        Số hóa đơn: # ${posOrder?.orderNumber || 0}
                    </p>
                    <p style="font-size: 0.6875rem; margin-bottom: 0.125rem;">
                        NV: ${UserLoginInformation.fullname || ''}
                    </p>
                    <p style="font-size: 0.6875rem; margin-bottom: 0.125rem;">
                        Ngày: ${moment(posOrder?.orderDate).format('dd/MM/yyyy HH:mm')}
                    </p>
                    <p style="font-size: 0.6875rem; margin-bottom: 0.125rem;">
                        Bàn/Khách: ${ posOrder?.customersName || ''}
                    </p>
                    <p style="font-size: 0.6875rem; margin-bottom: 0.125rem;">
                        Loại đơn hàng: ${ posOrder?.orderTypeName }
                    </p>
                </div>
                <div style="
                  background-image: linear-gradient(
                    to right,
                    #818181 50%,
                    rgba(255, 255, 255, 0) 0%
                  );
                  background-position: bottom;
                  background-size: 10px 1px;
                  background-repeat: repeat-x;
                  padding-top: 0.25rem !important;
                  margin-top: 0.5rem !important;
                ">
                    <div style="
                    color: #000000;
                    font-size: 0.625rem;
                    width: 100%;
                    margin-bottom: 1rem;
                  ">
                        <div style="display: flex !important;">
                            <div style="width: 7%; margin-right: 0.6875rem;">
                                <p style="margin-bottom: 0.5rem;">STT</p>
                            </div>
                            <div style="width: 66%; margin-right: 0.9375rem;">
                                <p style="margin-bottom: 0.5rem;">Tên sản phẩm</p>
                            </div>
                            <div style="width: 4.5%; margin-right: 0.9375rem;">
                                <p style="margin-bottom: 0.5rem;">SL</p>
                            </div>
                        </div>
                        ${PrinterService.setItem2(posOrder.orderDetails || [])}
                    </div>
                </div>
            </div>
        </div>`
    }
    private static setItem2(items: OrderDetail[] = []): string {
        let itemsHtml = '';
        items?.forEach((item, i) => {
            itemsHtml +=`
            <div style="display: flex !important;">
                <div style="width: 7%; margin-right: 0.6875rem;">
                    <p style="margin-bottom: 0.5rem;">${ i + 1 }</p>
                </div>
                <div style="width: 66%; margin-right: 0.9375rem;">
                    <p style="margin-bottom: 0.5rem;">${ item.menuPriceName || ''}</p>
                </div>
                <div style="
          width: 4.5%;
          margin-right: 0.9375rem;
          text-align: center !important;
        ">
                    <p style="margin-bottom: 0.5rem;">${ item.itemQuantity || 0 }</p>
                </div>
            </div>
            <div *ngIf="item.orderDetailNote" style="
        margin-top: -0.5rem;
        padding-bottom: 0.75rem;
        display: flex !important;
      ">
                <div style="width: 7%; margin-right: 0.6875rem;">
                    <p></p>
                </div>
                <div style="
          width: 93%;
          color: #929292;
          margin-right: 0.9375rem;
          text-align: left !important;
        ">
                    <i>${ item.orderDetailNote || ''}</i>
                </div>
            </div>
        `});
        return itemsHtml || '';
    }
}

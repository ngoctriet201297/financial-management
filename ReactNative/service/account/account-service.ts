import api from '../rest-service/rest-service';
import { UserLoginInput } from '../../models/user-login/user-login-input';
import { UserLoginOutput } from '../../models/user-login/user-login-output';
import { AxiosResponse } from 'axios';
import { Md5 } from '../../utility/md5/md5';

export class AccountService {
    static login(userLogin: UserLoginInput) : Promise<AxiosResponse<UserLoginOutput>>{
       
        userLogin.password = Md5(userLogin?.password || '');
        return api.post<UserLoginOutput>(
            'Authorization',
            userLogin).then(resp => {
                return resp;
            });
    }
}

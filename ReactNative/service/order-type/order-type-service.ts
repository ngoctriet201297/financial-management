import api from '../rest-service/rest-service';
import { AxiosResponse } from 'axios';
import { OrderType } from '../../models/OrderType/OrderType';

export class OrderTypeService {
    static getAll() : Promise<AxiosResponse<OrderType[]>>{

        return api.get<OrderType[]>(
            'api/ecoffee/ordertype').then(resp => {
                return resp;
            });
    }
}

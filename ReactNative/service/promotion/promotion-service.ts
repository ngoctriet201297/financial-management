import api from '../rest-service/rest-service';
import { AxiosResponse } from 'axios';
import { OrderType } from '../../models/OrderType/OrderType';
import { Promotion } from '../../models/Promotion/Promotion';

export class PromotionService {
    static getAll() : Promise<AxiosResponse<Promotion[]>>{

        return api.get<Promotion[]>(
            'api/ecoffee/promotion').then(resp => {
                return resp;
            });
    }
}

import api from '../rest-service/rest-service';
import { AxiosResponse } from 'axios';
import { Customer } from '../../models/customer/customer';

export class CustomerService {
    static getAll() : Promise<AxiosResponse<Customer[]>>{

        return api.get<Customer[]>(
            'api/ecoffee/customers').then(resp => {
                return resp;
            });
    }
}

import api from '../rest-service/rest-service';
import { AxiosResponse } from 'axios';
import { Menu } from '../../models/menu/menu';

export class MenuService {
    static getAll() : Promise<AxiosResponse<Menu[]>>{

        return api.get<Menu[]>(
            'api/ecoffee/pos/menu').then(resp => {
                return resp;
            });
    }
}

import api from '../rest-service/rest-service';
import { AxiosResponse } from 'axios';
import { Menu } from '../../models/menu/menu';
import { Order } from '../../models/order/order';

export class OrderService {
    static getNew() : Promise<AxiosResponse<Order>>{

        return api.post<Order>(
            'api/ecoffee/pos/neworder').then(resp => {
                return resp;
            });
    }
    static cancel(input: Order) : Promise<AxiosResponse<boolean>>{
        return api.post<boolean>(
            'api/ecoffee/pos/cancelorder',input).then(resp => {
                return resp;
            });
    }
    static place(input: Order) : Promise<AxiosResponse<boolean>>{
        return api.post<boolean>(
            'api/ecoffee/pos/placeorder',input).then(resp => {
                return resp;
            });
    }
}

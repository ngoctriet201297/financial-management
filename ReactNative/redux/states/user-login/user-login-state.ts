export default interface IUserLoginState{
    username?: string;
    token?: string;
} 
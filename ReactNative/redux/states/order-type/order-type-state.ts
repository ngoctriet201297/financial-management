import { OrderType } from "../../../models/ordertype/ordertype";

export default interface IOrderTypeState{
    orderTypes: OrderType[]
}
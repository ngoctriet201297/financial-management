import { Menu } from "../../../models/menu/menu";
import { GroupMenu } from "../../../models/menu/group-menu";

export default interface IMenuState{
    menus: Menu[],
    groups: GroupMenu[]
}
import IUserLoginState from "./user-login/user-login-state";
import ILoadingState from "./loading/loading-state";
import ICustomerState from "./customer/customer-state";
import IMenuState from "./menu/menu-state";
import IOrderState from "./order/order-state";
import IPromotionState from "./promotion/promotion-state";
import IOrderTypeState from "./order-type/order-type-state";

export interface IAppState{
    userLogin: IUserLoginState,
    loading: ILoadingState,
    customer: ICustomerState,
    menu: IMenuState,
    order: IOrderState,
    promotion: IPromotionState,
    orderType: IOrderTypeState
 }
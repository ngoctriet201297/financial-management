import { Customer } from "../../../models/customer/customer";

export default interface ICustomerState{
    customers: Customer[],
    choosenCustomer?: Customer
}
import { IDictionary } from "../../../utility/i-dictionary";

export default interface ILoadingState{
    loading: boolean,
    actives?: IDictionary<any>
}
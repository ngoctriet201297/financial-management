import { Promotion } from "../../../models/promotion/promotion";

export default interface IPromotionState{
    promotions: Promotion[]
}
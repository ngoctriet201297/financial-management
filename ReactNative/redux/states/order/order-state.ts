import { Order } from "../../../models/order/order";
import { CreateGroupMenu } from "../../../models/order/create-group-menu";

export default interface IOrderState{
   groupMenus?: CreateGroupMenu[];
   order?: Order,
   filter: string,
   billUri?: string;
} 
import { IAction } from "../common";
import {UserLoginInput} from '../../../models/user-login/user-login-input';
import { UserLoginOutput } from "../../../models/user-login/user-login-output";

export class UserLoginAction{
    
    public static readonly LOGIN = "LoginAction.LOGIN";
    public static readonly LOGIN_SUCCESS = "LoginAction.LOGIN_SUCCESS";
    public static readonly LOGOUT = "LoginAction.LOGOUT";
    public static readonly CLEAR_TOKEN = "LoginAction.CLEAR_TOKEN";

    static Login(input: UserLoginInput): IAction<UserLoginInput> {
        return {
            type: UserLoginAction.LOGIN,
            data: input
        };
    };

    static Logout(): IAction<void>{
        return {
            type: UserLoginAction.LOGOUT
        }
    }

    static LoginSuccess(input: UserLoginOutput): IAction<UserLoginOutput>{
        return {
            type: UserLoginAction.LOGIN_SUCCESS,
            data: input
        }
    }

    static clearToken(): IAction<void>{
        return {
            type: this.CLEAR_TOKEN
        }
    }
}
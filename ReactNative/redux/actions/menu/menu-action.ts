import {IAction} from '../common';
import { Menu } from '../../../models/menu/menu';

export class MenuAction{

    public static readonly GET_ALL = "MenuAction.GET_ALL";
    public static readonly GET_ALL_SUCCESS = "MenuAction.GET_ALL_SUCCESS";

    static getAll(): IAction<null>{
        return {
            type: MenuAction.GET_ALL
        }
    }
    static getAllSuccess(input: Menu[]): IAction<Menu[]>{
        return {
            type: MenuAction.GET_ALL_SUCCESS,
            data: input
        }
    }
}
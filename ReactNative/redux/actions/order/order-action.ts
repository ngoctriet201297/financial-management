import {IAction} from '../common';
import { Order } from '../../../models/order/order';
import { CreateGroupMenu } from '../../../models/order/create-group-menu';
import { CreateMenu } from '../../../models/order/create-order';
import { Customer } from '../../../models/customer/customer';

export class OrderAction{

    public static readonly SET_ORDER = "OrderAction.SET_ORDER";
    public static readonly CREATE_ORDER = "OrderAction.CREATE_ORDER";
    public static readonly CANCEL_ORDER = "OrderAction.CANCEL_ORDER";
    public static readonly SET_DETAIL_ORDER = "OrderAction.SET_DETAIL_ORDER";
    public static readonly SET_FILTER_ORDER = "OrderAction.SET_FILTER_ORDER";
    public static readonly CLEAR = "OrderAction.CLEAR";
    public static readonly UPDATE_ITEM = "OrderAction.UPDATE_ITEM";
    public static readonly PLACE_ORDER = "OrderAction.PLACE_ORDER";
    public static readonly SET_BILL_URI = "OrderAction.SET_BILL_URI";

    static setOrder(input: Order): IAction<Order>{
        return {
            type: OrderAction.SET_ORDER,
            data: input
        }
    }
    static createOrder(groupMenus: CreateGroupMenu[], customer: Customer): IAction<any>{
        return {
            type: OrderAction.CREATE_ORDER,
            data: {
                groupMenus,
                customer
            }
        }
    }
    static setDetailOrder(data: CreateGroupMenu[]): IAction<CreateGroupMenu[]>{
        return {
            type: OrderAction.SET_DETAIL_ORDER,
            data
        }
    }
    static setFilterOrder(data: string): IAction<string>{
        return {
            type: OrderAction.SET_FILTER_ORDER,
            data
        }
    }
    static cancelOrder(data: Order): IAction<Order>{
        return {
            type: OrderAction.CANCEL_ORDER,
            data
        } 
    }
    static clear(): IAction<null>{
        return {
            type: OrderAction.CLEAR
        }
    }
    static updateItem(data: CreateMenu): IAction<CreateMenu>{
        return {
            type: OrderAction.UPDATE_ITEM,
            data
        }
    }
    static placeOrder(data: Order): IAction<Order>{
        return {
            type: OrderAction.PLACE_ORDER,
            data
        }
    }
    static setBillUri(data: string): IAction<string>{
        return {
            type: OrderAction.SET_BILL_URI,
            data
        }
    }
}
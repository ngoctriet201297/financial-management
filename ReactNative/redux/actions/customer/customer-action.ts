import {IAction} from '../common';
import { Customer } from '../../../models/customer/customer';

export class CustomerAction{

    public static readonly GET_ALL = "CustomerAction.GET_ALL";
    public static readonly GET_ALL_SUCCESS = "CustomerAction.GET_ALL_SUCCESS";
    public static readonly SET_CUSTOMER = "CustomerAction.SET_CUSTOMER";

    static getAll(): IAction<null>{
        return {
            type: CustomerAction.GET_ALL
        }
    }

    static getAllSuccess(input: Customer[]): IAction<Customer[]>{
        return {
            type: CustomerAction.GET_ALL_SUCCESS,
            data: input
        }
    }

    static setCustomer(input: Customer): IAction<Customer>{
        return {
            type: CustomerAction.SET_CUSTOMER,
            data: input
        }
    }
}
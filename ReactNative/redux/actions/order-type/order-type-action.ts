import {IAction} from '../common';
import { Order } from '../../../models/order/order';
import { OrderType } from '../../../models/ordertype/ordertype';

export class OrderTypeAction{

    public static readonly GET_ALL = "OrderTypeAction.GET_ALL";
    public static readonly GET_ALL_SUCCESS = "OrderTypeAction.GET_ALL_SUCCESS";

    static getAll(): IAction<null>{
        return {
            type: OrderTypeAction.GET_ALL
        }
    }
    static getAllSuccess(input: OrderType[]): IAction<OrderType[]>{
        return {
            type: OrderTypeAction.GET_ALL_SUCCESS,
            data: input
        }
    }
}
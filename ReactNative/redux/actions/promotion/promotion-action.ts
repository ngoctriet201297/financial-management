import {IAction} from '../common';
import { Promotion } from '../../../models/promotion/promotion';

export class PromotionAction{

    public static readonly GET_ALL = "PromotionAction.GET_ALL";
    public static readonly GET_ALL_SUCCESS = "PromotionAction.GET_ALL_SUCCESS";

    static getAll(): IAction<null>{
        return {
            type: PromotionAction.GET_ALL
        }
    }
    static getAllSuccess(input: Promotion[]): IAction<Promotion[]>{
        return {
            type: PromotionAction.GET_ALL_SUCCESS,
            data: input
        }
    }
}
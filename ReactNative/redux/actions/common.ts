export interface IAction<Data>{
    type: string;
    data?: Data;
    callBack?: Function;
}
import {IAction} from '../common';
import { StartLoading, StopLoading } from '../../../models/loading';

export class LoadingAction{

    public static readonly START_LOADING = "LoadingAction.START_LOADING";
    public static readonly STOP_LOADING = "LoadingAction.STOP_LOADING";
    public static readonly CLEAR_LOADING = "LoadingAction.CLEAR_LOADING"

    static startLoading(input: StartLoading): IAction<StartLoading>{
        return {
            type: LoadingAction.START_LOADING,
            data: input
        }
    }
    static stopLoading(input: StopLoading): IAction<StopLoading>{
        return {
            type: LoadingAction.STOP_LOADING,
            data: input
        }
    }
    static clearLoading(): IAction<void>{
        return {
            type: LoadingAction.CLEAR_LOADING
        }
    }
}
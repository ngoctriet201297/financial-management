import { OrderService } from '../../service/order/order-service';
import { OrderAction } from '../actions/order/order-action';
import { IAction } from '../actions';
import { LoadingAction } from '../actions/loading/loading-action';
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { navigate } from '../../service/navigation/navigation-service';
import { Order } from '../../models/order/order';
import { HrisNotify } from '../../utility/hris-notify';
import moment from 'moment';
import { PrinterService } from '../../service/printer-bill/printer-service';
import { CustomerAction } from '../actions/customer/customer-action';

function* getNewOrder(action: IAction<any>) {
    yield put(LoadingAction.startLoading({
        key: 'getNewOrder'
    }));
    let resp = yield call(OrderService.getNew);
    if (resp?.data) {
        yield put(OrderAction.setOrder({
            ...resp.data,
            ...action?.data?.customer,
            customersId: action?.data?.customer.id
        }));
        yield put(OrderAction.setDetailOrder(action?.data?.groupMenus || []));
        yield put(CustomerAction.setCustomer(action?.data?.customer));
        yield navigate('Menu');
    }
    yield put(LoadingAction.stopLoading({
        key: 'getNewOrder'
    }));
}

function* cancelOrder(action: IAction<Order>) {
    yield put(LoadingAction.startLoading({
        key: 'cancelOrder'
    }));
    if (!action?.data) {
        HrisNotify.warn('Hủy đơn hàng thất bại');
        yield put(LoadingAction.stopLoading({
            key: 'cancelOrder'
        }));
        return;
    }
    let resp = yield call(OrderService.cancel, action!.data);
    if (resp?.data) {
        yield put(OrderAction.clear());
        yield navigate('Table');
    }
    yield put(LoadingAction.stopLoading({
        key: 'cancelOrder'
    }));
}

function* placeOrder(action: IAction<Order>) {
    yield put(LoadingAction.startLoading({
        key: 'placeOrder'
    }));
    if (!action?.data) {
        HrisNotify.warn('Hủy đơn hàng thất bại');
        yield put(LoadingAction.stopLoading({
            key: 'placeOrder'
        }));
        return;
    }
    action.data.orderDate = moment().format();
    yield put(OrderAction.setOrder(action.data));
    let resp = yield call(OrderService.place, action.data);
    if (resp?.data) {
        HrisNotify.success('Thêm đơn hàng thành công');
        navigate('Printer');
        //navigate('Table');
    }
    yield put(LoadingAction.stopLoading({
        key: 'placeOrder'
    }));
}

export function* orderSaga() {
    yield all([
        takeLatest(OrderAction.CREATE_ORDER, getNewOrder),
        takeLatest(OrderAction.CANCEL_ORDER, cancelOrder),
        takeLatest(OrderAction.PLACE_ORDER, placeOrder)
    ])
}
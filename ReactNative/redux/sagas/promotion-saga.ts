import { call, put, takeLatest, all } from 'redux-saga/effects';
import { IAction } from '../actions';
import { LoadingAction } from '../actions/loading/loading-action';
import { PromotionAction } from '../actions/promotion/promotion-action';
import { PromotionService } from '../../service/promotion/promotion-service';

function* getPromotions(action: IAction<null>){
    yield put(LoadingAction.startLoading({
        key: 'getPromotions'
    }));
    let resp = yield call(PromotionService.getAll);
    if(resp?.data){
        yield put(PromotionAction.getAllSuccess(resp.data));
    }

    yield put(LoadingAction.stopLoading({
        key: 'getPromotions'
    })); 
}

export function* PromotionSaga() {
    yield all([
      takeLatest(PromotionAction.GET_ALL, getPromotions)
    ]);
  }
  

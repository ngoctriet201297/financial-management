import { all, fork } from 'redux-saga/effects';
import { AccountSaga } from './account-saga';
import { CustomerSaga } from './customer-saga';
import { MenuSaga } from './menu-saga';
import { OrderTypeSaga } from './order-type-saga';
import { PromotionSaga } from './promotion-saga';
import { orderSaga } from './order-saga';

export default function* rootSaga(){
    yield all([
        fork(AccountSaga),
        fork(CustomerSaga),
        fork(MenuSaga),
        fork(OrderTypeSaga),
        fork(PromotionSaga),
        fork(orderSaga)
    ])
}
import { call, put, takeLatest, all } from 'redux-saga/effects';
import { IAction, UserLoginAction } from '../actions';
import { UserLoginInput } from '../../models/user-login/user-login-input';
import { LoadingAction } from '../actions/loading/loading-action';
import { AccountService } from '../../service/account/account-service';
import { HrisNotify } from '../../utility/hris-notify';
import { LocalStorageHelper } from '../../utility/local-storage-helper/local-storage-helper';

function* login(action: IAction<UserLoginInput>){
    yield put(LoadingAction.startLoading({
        key: 'Login'
    }));
    if(!action?.data){
        yield put(LoadingAction.stopLoading({
            key: 'UserLogin'
        })); 
        HrisNotify.warn('Tài khoản và mật khẩu không để rỗng');
        return;
    }
    let userLogin = yield call(AccountService.login,action?.data || {password: '', username: ''});
    if(userLogin){
        yield put(UserLoginAction.LoginSuccess({
            token: userLogin?.data?.token,
            username: userLogin.data?.username
        }));
        yield LocalStorageHelper.setUserLogin({
            username: userLogin.data?.username,
            token: userLogin.data?.token
        })
    }
    yield put(LoadingAction.stopLoading({
        key: 'Login'
    })); 
}

export function* AccountSaga() {
    yield all([
      takeLatest(UserLoginAction.LOGIN, login)
    ]);
  }
  

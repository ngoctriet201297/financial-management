import { call, put, takeLatest, all } from 'redux-saga/effects';
import { IAction } from '../actions';
import { LoadingAction } from '../actions/loading/loading-action';
import { MenuAction } from '../actions/menu/menu-action';
import { MenuService } from '../../service/menu/menu-service';

function* getMenus(action: IAction<null>){
    yield put(LoadingAction.startLoading({
        key: 'getMenus'
    }));
    let resp = yield call(MenuService.getAll);
    if(resp?.data){
        yield put(MenuAction.getAllSuccess(resp.data));
    }
    yield put(LoadingAction.stopLoading({
        key: 'getMenus'
    })); 
}

export function* MenuSaga() {
    yield all([
      takeLatest(MenuAction.GET_ALL, getMenus)
    ]);
  }
  

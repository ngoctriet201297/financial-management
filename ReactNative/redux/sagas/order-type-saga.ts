import { call, put, takeLatest, all } from 'redux-saga/effects';
import { IAction } from '../actions';
import { LoadingAction } from '../actions/loading/loading-action';
import { OrderTypeAction } from '../actions/order-type/order-type-action';
import { OrderTypeService } from '../../service/order-type/order-type-service';

function* getOrderTypes(action: IAction<null>){
    yield put(LoadingAction.startLoading({
        key: 'getOrderTypes'
    }));
    let resp = yield call(OrderTypeService.getAll);
    if(resp?.data){
        yield put(OrderTypeAction.getAllSuccess(resp.data));
    }

    yield put(LoadingAction.stopLoading({
        key: 'getOrderTypes'
    })); 
}

export function* OrderTypeSaga() {
    yield all([
      takeLatest(OrderTypeAction.GET_ALL, getOrderTypes)
    ]);
  }
  

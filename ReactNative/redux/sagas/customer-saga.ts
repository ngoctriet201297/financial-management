import { call, put, takeLatest, all } from 'redux-saga/effects';
import { IAction } from '../actions';
import { LoadingAction } from '../actions/loading/loading-action';
import { CustomerService } from '../../service/customer/customer-service';
import { CustomerAction } from '../actions/customer/customer-action';
import { Customer } from '../../models/customer/customer';

function* getCustomers(action: IAction<null>){
    yield put(LoadingAction.startLoading({
        key: 'getCustomers'
    }));
    let resp = yield call(CustomerService.getAll);
    if(Array.isArray(resp?.data || '')){
        yield put(CustomerAction.getAllSuccess(resp.data));
    }

    yield put(LoadingAction.stopLoading({
        key: 'getCustomers'
    })); 
}

export function* CustomerSaga() {
    yield all([
      takeLatest(CustomerAction.GET_ALL, getCustomers)
    ]);
  }
  

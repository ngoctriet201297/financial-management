import { IAction } from "../../actions/common";
import IPersistentStorageState from "../../states/loading/loading-state";
import ILoadingState from "../../states/loading/loading-state";
import {StopLoading,StartLoading} from '../../../models/loading';
import { LoadingAction } from "../../actions/loading/loading-action";

export default class LoadingReducer{

    private static readonly initalValue: ILoadingState = {
        loading: false
    }
    
    public static reducer(state: ILoadingState = LoadingReducer.initalValue, action: InputLoadingAction) :
    IPersistentStorageState{
       
        switch(action.type){
            case LoadingAction.START_LOADING:
                return {
                    ...state,
                    loading: true,
                    actives: {...state.actives, [action?.data?.key || '']: action}
                };
            case LoadingAction.STOP_LOADING:
                if(Object.keys(state?.actives || {}).length){
                    delete state.actives![action?.data?.key || ''];
                }
                return {
                    ...state,
                    loading: !!Object.keys(state?.actives || {}).length
                }
            default:
                return state;
        }
    }
}

 declare type InputLoadingAction = IAction<StartLoading> | IAction<StopLoading>
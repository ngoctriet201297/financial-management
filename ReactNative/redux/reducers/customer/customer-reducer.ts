import { IAction } from "../../actions/common";
import ICustomerState from "../../states/customer/customer-state";
import { Customer } from "../../../models/customer/customer";
import { CustomerAction } from "../../actions/customer/customer-action";

export default class CustomerReducer{

    private static readonly initalValue: ICustomerState = {
        customers: []
    }
    
    public static reducer(state: ICustomerState = CustomerReducer.initalValue, action: any) :
    ICustomerState{
       
        switch(action.type){
            case CustomerAction.GET_ALL_SUCCESS:
                return {
                    ...state,
                   customers: action?.data || []
                };
            case CustomerAction.SET_CUSTOMER:
                return {
                    ...state,
                    choosenCustomer: action?.data
                }
            default:
                return state;
        }
    }
}

 declare type InputLoadingAction = IAction<Customer[]> ;
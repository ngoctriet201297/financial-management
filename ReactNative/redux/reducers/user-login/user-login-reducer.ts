import UserLoginState from "../../states/user-login/user-login-state";
import { IAction } from "../../actions/common";
import {UserLogin} from "../../../models/user-login";
import { UserLoginAction } from "../../actions/user-login/user-login-action";
import { UserLoginOutput } from "../../../models/user-login/user-login-output";
import { UserLoginInformation } from "../../../utility/UserLoginInformation";

export default class UserLoginReducer{

    private static readonly initalValue: UserLoginState = {
        token: '',
        username: ''
    }
    
    public static reducer(state: UserLoginState = UserLoginReducer.initalValue, action: InputUserLoginAction) : UserLoginState{   
        switch(action.type){
            case UserLoginAction.LOGIN_SUCCESS:
                  UserLoginInformation.fullname = action.data?.fullname || '';
                return {
                    ...state,
                    username: action.data?.username,
                    token: action.data?.token
                };
            case UserLoginAction.LOGOUT:
                return {
                    ...state,
                    token: ''
                }
            default:
                return state;
        }
    }
}

 declare type InputUserLoginAction = IAction<UserLoginOutput>
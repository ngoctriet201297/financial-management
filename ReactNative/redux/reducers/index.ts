import UserLoginReducer from './user-login/user-login-reducer';
import LoadingReducer from './loading/loading-reducer';
import CustomerReducer from './customer/customer-reducer';
import MenuReducer from './menu/menu-reducer';
import OrderReducer from './order/order-reducer';
import PromotionReducer from './promotion/promotion-reducer';
import OrderTypeReducer from './order-type/order-type-reducer';

export default{
   userLogin: UserLoginReducer.reducer,
   loading: LoadingReducer.reducer,
   customer: CustomerReducer.reducer,
   menu: MenuReducer.reducer,
   order: OrderReducer.reducer,
   promotion: PromotionReducer.reducer,
   orderType: OrderTypeReducer.reducer
};
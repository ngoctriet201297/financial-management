import { IAction } from "../../actions/common";
import { OrderType } from "../../../models/OrderType/OrderType";
import IOrderTypeState from "../../states/order-type/order-type-state";
import { OrderTypeAction } from "../../actions/order-type/order-type-action";

export default class OrderTypeReducer{

    private static readonly initalValue: IOrderTypeState = {
        orderTypes: []
    }
    
    public static reducer(state: IOrderTypeState = OrderTypeReducer.initalValue, action: InputLoadingAction) :
    IOrderTypeState{
       
        switch(action.type){
            case OrderTypeAction.GET_ALL_SUCCESS:
                
                return {
                    ...state,
                    orderTypes: action?.data || []
                };
            default:
                return state;
        }
    }
}

 declare type InputLoadingAction = IAction<OrderType[]> ;
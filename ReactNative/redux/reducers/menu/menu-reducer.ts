import { IAction } from "../../actions/common";
import IMenuState from "../../states/menu/menu-state";
import { MenuAction } from "../../actions/menu/menu-action";
import { Menu } from "../../../models/menu/menu";
import { GroupMenu } from "../../../models/menu/group-menu";

export default class MenuReducer {

    private static readonly initalValue: IMenuState = {
        menus: [],
        groups: []
    }

    private static getGroupMenu(menus?: Menu[]): GroupMenu[] {   
        if (!Array.isArray(menus || '')) return [];
        let groupsMenus = menus!.map((menu, index, selfMenu) => {
            return new GroupMenu({
                code: menu.groupProductCode,
                name: menu.groupProductName,
                id: menu.groupProductId,
                menus: selfMenu.filter(item => item.groupProductId === menu.groupProductId)
            })
        }).filter((value, index, self) => self.findIndex(s => s.id === value.id) === index)
        .sort((a, b) => {
            if ((a?.name || '') < (b?.name || '')) { return -1; }
            if ((a?.name || '') > (b?.name || '')) { return 1; }
            return 0;
        });
        return groupsMenus;
    }

    public static reducer(state: IMenuState = MenuReducer.initalValue, action: InputLoadingAction):
        IMenuState {
        switch (action.type) {
            case MenuAction.GET_ALL_SUCCESS:             
                return {
                    ...state,
                    menus: action?.data || [],
                    groups: MenuReducer.getGroupMenu(action?.data)
                };
            default:
                return state;
        }
    }
    
}

declare type InputLoadingAction = IAction<Menu[]>;
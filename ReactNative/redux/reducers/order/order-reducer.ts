import { IAction } from "../../actions/common";
import IOrderState from "../../states/order/order-state";
import { OrderAction } from "../../actions/order/order-action";
import { Order } from "../../../models/order/order";
import { CreateGroupMenu } from "../../../models/order/create-group-menu";
import { CreateMenu } from "../../../models/order/create-order";

export default class OrderReducer{

    private static readonly initalValue: IOrderState = {
        groupMenus: [],
        filter: ''
    }
    
    public static reducer(state: IOrderState = OrderReducer.initalValue, action: any) : IOrderState{   
        switch(action.type){
            case OrderAction.SET_ORDER:
                return {
                    ...state,
                    order: action?.data
                };
            case OrderAction.SET_DETAIL_ORDER:
                return {
                    ...state,
                    groupMenus: action?.data
                }
            case OrderAction.SET_FILTER_ORDER:
                return {
                    ...state,
                    filter: action.data
                }
            case OrderAction.CLEAR:
                return {
                    filter: ''
                }
            case OrderAction.UPDATE_ITEM:               
                let groupMenus = JSON.parse(JSON.stringify(state.groupMenus || [])) as CreateGroupMenu[];        
                let neededGroupMenu = groupMenus?.find(groupMenu => groupMenu.id === action.data?.groupProductId);           
                let neededMenu = neededGroupMenu?.menus?.find(menu => menu.id === action.data?.id);

                let itemTotalAmount = (action.data?.itemQuantity || 0) * (action.data?.menuAmountSalling || 0);
                let itemDiscountTotal = (action.data?.discountAmout || 0) + ( ((action.data?.discountPer || 0)/100)*itemTotalAmount );
                let itemTotalAmountAfterDiscount = itemTotalAmount - itemDiscountTotal;

                if(neededMenu){
                    neededMenu.discountAmout = (action.data?.discountAmout || 0);
                    neededMenu.discountPer = (action.data?.discountPer || 0);
                    neededMenu.itemDiscountTotal = itemDiscountTotal;
                    neededMenu.itemQuantity = action.data?.itemQuantity;
                    neededMenu.itemTotalAmount = itemTotalAmount;
                    neededMenu.itemTotalAmountAfterDiscount = itemTotalAmountAfterDiscount;
                    neededMenu.orderId = action.data?.orderId;
                    neededMenu.promotionCode = action.data?.promotionCode;
                    neededMenu.promotionId = action.data?.promotionId;
                    neededMenu.promotionName = action.data?.promotionName;
                    neededMenu.orderDetailNote = action.data?.orderDetailNote;
                }
                return {
                    ...state,
                    groupMenus
                }
            case OrderAction.SET_BILL_URI:
                return {
                    ...state,
                    billUri: action.data
                }
            default:
                return state;
        }
    }
}
  type InputAction = IAction<CreateGroupMenu[]> | IAction<Order[]> | IAction<CreateMenu>
import { IAction } from "../../actions/common";
import { Promotion } from "../../../models/Promotion/Promotion";
import IPromotionState from "../../states/promotion/promotion-state";
import { PromotionAction } from "../../actions/promotion/promotion-action";

export default class PromotionReducer{

    private static readonly initalValue: IPromotionState = {
        promotions: []
    }
    
    public static reducer(state: IPromotionState = PromotionReducer.initalValue, action: InputLoadingAction) :
    IPromotionState{
       
        switch(action.type){
            case PromotionAction.GET_ALL_SUCCESS:
                
                return {
                    ...state,
                    promotions: action?.data || []
                };
            default:
                return state;
        }
    }
}

 declare type InputLoadingAction = IAction<Promotion[]> ;
import { createStore, combineReducers, applyMiddleware } from 'redux';
import reducers from '../redux/reducers';
import rootSaga from '../redux/sagas';
import createSagaMiddleware from 'redux-saga';
import { IAppState } from '../redux/states';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(){
    const store = createStore(
        combineReducers<IAppState>({...reducers}),
        applyMiddleware(sagaMiddleware)
    );
    sagaMiddleware.run(rootSaga);
    return store;
}
import { Toast } from 'native-base';
import api from '../service/rest-service/rest-service';
import { IAppState } from '../redux/states';
import { Store, CombinedState, AnyAction } from 'redux';
import { LoadingAction } from '../redux/actions/loading/loading-action';
import { UserLoginAction } from '../redux/actions';

export function initAPIInterceptor(store: Store<CombinedState<IAppState>, AnyAction>) {
    api.interceptors.request.use(
        async request => {
        
            const { loading, userLogin } = store.getState();
            
            if (!request.headers.Authorization && userLogin?.token) {
                request.headers.Authorization = 'bearer ' + userLogin.token;
            }

            if (!request.headers['Content-Type']) {
                request.headers['Content-Type'] = 'application/json';
            }
            return request;

        },
        error => console.log('error',error)
    );

    api.interceptors.response.use(
        response => {
            if(response?.data?.code === 200){
                return response?.data
            }
            store.dispatch(LoadingAction.clearLoading());
            showError({status: response?.data?.code});         
        },
        error => {
            store.dispatch(LoadingAction.clearLoading());
            const errorRes = error.response;
            if (errorRes) {
                if (errorRes.status === 401) {
                    store.dispatch(UserLoginAction.Logout())
                }
                showError({ error: errorRes.data.error, status: errorRes.status });
            } else {
                Toast.show({
                    text: 'Một lỗi không mong đợi đã xảy ra',
                    buttonText: 'x',
                    duration: 10000,
                    type: 'danger',
                    textStyle: { textAlign: 'center' },
                });
            }
        }
    )
}
function showError({ error = '', status = 500 }): void {
    let message = '';
    if (error) {
        message = error;
    } else {
        switch (status) {
            case 401:
                message = 'Bạn không có quyền';
                break;
            case 403:
                message = 'Bạn bị từ chối truy cập';
                break;
            case 404:
                message = 'Tài nguyên không tìm thấy';
                break;
            case 500:
                message = 'Lỗi máy chủ';
                break;
            default:
                message = 'Lỗi máy chủ';
                break;
        }
    }
    Toast.show({
        text: message,
        buttonText: 'X',
        duration: 10000,
        type: 'danger',
        textStyle: { textAlign: 'center' }
    })
}
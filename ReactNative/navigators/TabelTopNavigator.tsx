import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import AllTableScreen from '../screens/table/all-table/AllTableScreen';
import AllTableContainer from '../containers/table/AllTableContainer';
import UsedTableContainer from '../containers/table/UsedTableContainer';
import EmptyTableContainer from '../containers/table/EmptyTableContainer';
import { View } from 'react-native-animatable';
import { FontAwesome, Feather } from '@expo/vector-icons';
import { TouchableOpacity, Text } from 'react-native';
const Tab = createMaterialTopTabNavigator();

export default function TableTopNavigator(){
    return (
        <Tab.Navigator>
            <Tab.Screen options={{title: 'Tất cả'}} component={AllTableContainer} name="AllTable" ></Tab.Screen>
            <Tab.Screen options={{title: 'Sử dụng'}} component={UsedTableContainer} name="UsedTable"></Tab.Screen>
            <Tab.Screen options={{title: 'Còn trống'}} component={EmptyTableContainer} name="EmptyTable"></Tab.Screen>
        </Tab.Navigator>
    );
}

export function TableTopNavigatorHeader(props: any){
    return (
        <View style={{flex: 1, justifyContent: 'center', marginLeft: 10}}>
            <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
                <FontAwesome size={25} name="bars" color="#fff"/>
            </TouchableOpacity>
        </View>
    )
}
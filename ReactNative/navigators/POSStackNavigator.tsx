import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import TableTopNavigator, { TableTopNavigatorHeader } from './TabelTopNavigator';
import { LogoutScreen } from '../screens/logout/LogoutScreen';
import MenuContainer from '../containers/menu/MenuContainer';
import BillContainer from '../containers/bill/BillContainer';
import PaymentContainer from '../containers/payment/PaymentContainer';
import AllTableContainer from '../containers/table/AllTableContainer';
import WebViewScreen from '../screens/web-view/web-view-screen';

const Stack = createStackNavigator();

export default function POSStackNavigator() {
    return (<Stack.Navigator screenOptions={{
        headerStyle: {
            backgroundColor: '#f60'
        },
        headerTintColor: '#fff'
    }}>
        <Stack.Screen name="Table"
            options={({ navigation, route }) => ({
                headerTitle: 'Danh sách phòng bàn',
                headerLeft: props => <TableTopNavigatorHeader navigation={navigation} />
            })}
            component={AllTableContainer}></Stack.Screen>
        <Stack.Screen options={{
            headerTitle: 'Chọn món vào đơn',
            headerShown: false
        }} name="Menu" component={MenuContainer}></Stack.Screen>
        <Stack.Screen options={{
            headerShown: false,
            title: 'Hóa đơn'
        }} name="Bill" component={BillContainer}></Stack.Screen>
        <Stack.Screen options={{
            headerTitle: 'Thanh toán'
        }} name="Payment" component={PaymentContainer}></Stack.Screen>
        <Stack.Screen options={{
            headerTitle: 'Printer',
            headerShown: false
        }} name="Printer" component={WebViewScreen}></Stack.Screen>
    </Stack.Navigator>)
}
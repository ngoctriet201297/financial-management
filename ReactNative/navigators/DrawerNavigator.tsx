import React, { useEffect } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import POSStackNavigator from './POSStackNavigator';
import StoreContainer from '../containers/store/StoreContainer';
import LogoutContainer from '../containers/logout/logout-container';
import { connect } from 'react-redux';
import { IAppState } from '../redux/states';
import { CustomerAction } from '../redux/actions/customer/customer-action';
import { MenuAction } from '../redux/actions/menu/menu-action';
import { OrderTypeAction } from '../redux/actions/order-type/order-type-action';
import { PromotionAction } from '../redux/actions/promotion/promotion-action';

const Drawer = createDrawerNavigator()

type Props = {
  getCustomers:() => void,
  getMenus: () => void,
  getOrderTypes: () => void,
  getPromotions: () => void,
};

function DrawerNavigator(Props: Props) {

  useEffect(() => {
    Props.getCustomers();
    Props.getMenus();
    Props.getOrderTypes();
    Props.getPromotions();
  }, []);

  return (
    <Drawer.Navigator>
      <Drawer.Screen options={{
        title: 'Bán hàng'
      }} name="POS" component={POSStackNavigator}></Drawer.Screen>
      <Drawer.Screen options={{
        title: 'Cửa hàng'
      }} name="Store" component={StoreContainer}></Drawer.Screen>
       <Drawer.Screen options={{
        title: 'Đăng xuất'
      }} name="Logout" component={LogoutContainer}></Drawer.Screen>
    </Drawer.Navigator>
  );

}
function mapStateToProps(state: IAppState){
  return {

  }
}
function mapDispatchToProps(dispatch: any){
  return {
    getCustomers:() => dispatch(CustomerAction.getAll()),
    getMenus: () => dispatch(MenuAction.getAll()),
    getOrderTypes: () => dispatch(OrderTypeAction.getAll()),
    getPromotions: () => dispatch(PromotionAction.getAll()),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(DrawerNavigator);
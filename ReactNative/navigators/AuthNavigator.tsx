import { createStackNavigator } from "@react-navigation/stack";
import React from 'react';
import LoginContainer from '../containers/login/login-container';

const Stack = createStackNavigator();

export default function AuthStackNavigator() {

  return (
    <Stack.Navigator initialRouteName="Login" screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="Login"
        component={LoginContainer}
        options={() => ({
          title: 'Login',
        })}
      />
    </Stack.Navigator>
  );
}

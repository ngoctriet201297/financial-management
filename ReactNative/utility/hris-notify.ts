import { Toast } from "native-base";

export class HrisNotify {

    static warn(message: string): void {
        Toast.show({
            text: message,
            buttonText: 'x',
            duration: 10000,
            type: 'warning',
            textStyle: { textAlign: 'center' },
        });
    }

    static error(message: string): void {
        Toast.show({
            text: message,
            buttonText: 'x',
            duration: 10000,
            type: 'danger',
            textStyle: { textAlign: 'center' },
        });
    }

    static success(message: string): void {
        Toast.show({
            text: message,
            buttonText: 'x',
            duration: 10000,
            type: 'success',
            textStyle: { textAlign: 'center' },
        });
    }
}
export class FormatNumber{
    static currency(value: number): string{
        return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value);
    }
    static currencyInput(value: number, n= 2,x = 3, s = '.', c = ','): string{
         
        return new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3}).format(value)
    }
}
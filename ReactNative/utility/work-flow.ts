import { Customer } from "../models/customer/customer";
import { navigate } from "../service/navigation/navigation-service";

export function checkCustomerInvalid(customer: Customer): void{
     if(!customer?.id) navigate("Table");
}
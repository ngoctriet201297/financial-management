import { UserLogin } from "../../models/user-login";
import AsyncStorage from '@react-native-community/async-storage';

export class LocalStorageHelper{
    static async setUserLogin(input: UserLogin): Promise<void>{
       await AsyncStorage.setItem('userLogin', JSON.stringify(input ?? undefined) );
    }
    static async getUserLogin(): Promise<UserLogin | null>{
        var userLogin = await AsyncStorage.getItem('userLogin');
        if(userLogin){
            return JSON.parse(userLogin) as UserLogin;
        }
        return null;
    }
}
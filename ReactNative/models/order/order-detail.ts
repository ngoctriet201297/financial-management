import BaseModel from "../base-model";

export class OrderDetail extends BaseModel {
    discountAmout!: number;
    discountPer!: number;
    groupProductCode!: string;
    groupProductId!: number;
    groupProductName!: string;
    itemDiscountTotal!: number;
    itemQuantity!: number;
    itemTotalAmount!: number;
    itemTotalAmountAfterDiscount!: number;
    listProductCode!: string;
    listProductId!: number;
    listProductName!: string;
    menuAmountCostofProduct!: number;
    menuAmountSalling!: number;
    menuPriceCode!: string;
    menuPriceId!: number;
    menuPriceName!: string;
    orderId!: number;
    promotionCode?: string;
    promotionId?: number;
    promotionName?: string;
    unitsCode!: string;
    unitsId!: number;
    unitsName!: string;
    orderDetailNote?: string;
    
    constructor(initalValue: Partial<OrderDetail> = {}) {
        super(initalValue);
    }
}
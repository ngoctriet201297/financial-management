import BaseModel from "../base-model";
import { OrderDetail } from "./order-detail";

export class Order extends BaseModel {
    customersCode?: string;
    customersId?: number;
    customersMoney?: number;
    customersName?: string;
    exchangecustomersMoney?: number;
    orderDate?: Date | string;
    orderDetails?: OrderDetail[]
    orderMonth?: number;
    orderNote?: string;
    orderNumber?: string;
    orderStatus?: number;
    orderTypeCode?: string;
    orderTypeId?: string;
    orderTypeName?: string;
    orderYear?: number;
    totalAmount?: number;
    totalDiscount?: number;
    totalPrice?: number;
    usersOrder?: any;
    totalQuantity?: number;
    
    constructor(initalValue: Partial<Order> = {}){
        super(initalValue);
    }
}
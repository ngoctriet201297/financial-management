import { Menu } from "../menu/menu";

export class CreateMenu extends Menu{
    
    discountAmout?: number;
    discountPer?: number;
    itemDiscountTotal?: number;
    itemQuantity?: number;
    itemTotalAmount?: number;
    itemTotalAmountAfterDiscount?: number;
    orderId?: number;
    promotionCode?: string;
    promotionId?: number;
    promotionName?: string;
    orderDetailNote?: string;

    constructor(initalValue: Partial<CreateMenu> = {}){
        super(initalValue);
    }
   
} 
import BaseModel from "../base-model"
import { CreateMenu } from "./create-order";

export class CreateGroupMenu extends BaseModel{
    
    name?: string;
    code?: string;
    menus?: CreateMenu[];

    constructor(initalValue: Partial<CreateGroupMenu> = {}){
        super(initalValue);
    }
   
} 
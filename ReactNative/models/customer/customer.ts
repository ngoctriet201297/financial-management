import BaseModel from "../base-model"

export class Customer extends BaseModel{
    
    customersAddress?: string;
    customersCode!: string;
    customersEmail?: string;
    customersName?: string;
    customersNamePhone?: string;

    constructor(initalValue: Partial<Customer> = {}){
        super(initalValue);
    }
   
} 
export interface UserLoginOutput{
    username?: string;
    token?: string;
    fullname?: string;
}
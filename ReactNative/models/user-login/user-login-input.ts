import BaseModel from '../base-model';
export interface UserLoginInput {
    username: string | undefined;
    password: string | undefined;
}
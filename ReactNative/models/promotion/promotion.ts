import BaseModel from "../base-model";

export class Promotion extends BaseModel {
    amount!: number;
    expired!: boolean;
    percentage!: number;
    promotionCode!: string;
    promotionDate!: Date;
    promotionExpDate!: Date;
    promotionMonth!: number;
    promotionName!: string;
    promotionNote?: string;
    promotionYear!: number;

    constructor(initalValue: Partial<Promotion> = {}){
        super(initalValue);
    }
}
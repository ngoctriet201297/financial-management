import BaseModel from "../base-model";

export class OrderType extends BaseModel {
    orderTypeCode!: string;
    orderTypeName!: string;

    constructor(initalValue: Partial<OrderType> = {}){
        super(initalValue);
    }
}
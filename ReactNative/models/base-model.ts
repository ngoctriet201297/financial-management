export default class BaseModel{
    id?: number | string;
    createdAt!: Date;
    createdBy!: string;
    deletedAt?: Date;
    deletedBy?: string;
    updatedAt?: Date;
    updatedBy?: string;
    constructor(initalValue: Partial<BaseModel> = {}){
        Object.keys(initalValue).forEach((key) => {
            var caller = this as any;
            var params = initalValue as any;
            caller[key] = params[key];
        });
    }
} 
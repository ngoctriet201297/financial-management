import BaseModel from "../base-model"
import { Menu } from "./menu";

export class GroupMenu extends BaseModel{
    
    name?: string;
    code?: string;
    menus?: Menu[];
    
    constructor(initalValue: Partial<GroupMenu> = {}){
        super(initalValue);
        
    }
   
} 
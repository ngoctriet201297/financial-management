import BaseModel from "../base-model"

export class Menu extends BaseModel{
    
    effectiveDate!: Date;
    expired!: boolean;
    expiredDate!: Date;
    groupProductCode!: string;
    groupProductId!: number;
    groupProductName!: string;
    listProductCode!: string;
    listProductId!: number;
    listProductName!: string;
    menuAmountCostofProduct!: number;
    menuAmountSalling!: number;
    menuPriceCode!: string;
    menuPriceDescription!: string;
    menuPriceName!: string;
    unitsCode!: string;
    unitsId!: number;
    unitsName!: string;

    constructor(initalValue: Partial<Menu> = {}){
        super(initalValue);
    }
   
} 
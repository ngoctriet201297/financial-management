import React, { useEffect } from 'react';
import { Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';

type Props = {
    logOut: () => any
}
export function LogoutScreen({ logOut }: Props) {

    const navigation = useNavigation();

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            Alert.alert('Bạn có chắc đăng xuất', '',
                [{
                    text: "Hủy bỏ",
                    onPress: () => navigation.goBack(),
                    style: "cancel"
                },
                { text: "Đồng ý", onPress: () => logOut() }]);
        });

        return unsubscribe;
    }, [navigation]);


    return null;
}
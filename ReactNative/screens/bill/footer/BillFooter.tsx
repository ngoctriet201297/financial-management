import React, { useState } from 'react';
import { View, Text } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { FontAwesome } from '@expo/vector-icons';
import { Styles } from './style';
import { useNavigation } from '@react-navigation/native';
import { GlobalStyles } from '../../../shared/styles/global-style';
import { Order } from '../../../models/order/order';
import { FormatNumber } from '../../../utility/format-numbet';

type Props = {
    order: Order
}

export function BillFooter({ order }: Props) {
    const navigation = useNavigation();

    let localQuantity = 0;
    let localAmount = 0;
    order?.orderDetails?.forEach(orderDetail => {
        localQuantity += (orderDetail.itemQuantity || 0);
        localAmount += (orderDetail.itemTotalAmount || 0);
    });
    
    return (
        <View style={Styles.container}>
            <View style={Styles.top}>
                <Text style={Styles.top_amount}>{localQuantity}</Text>
                <Text style={Styles.top_money}>{FormatNumber.currency(localAmount)}</Text>
                <FontAwesome size={20} name="money" color="gray" />
            </View>
            <View style={Styles.bottom}>
                <View style={Styles.bottom_payment}>
                    <TouchableOpacity style={Styles.bottom_btn} onPress={() => navigation.navigate('Payment')}>
                        <Text style={[GlobalStyles.color_white, GlobalStyles.text_size_20]}>Thanh toán</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}
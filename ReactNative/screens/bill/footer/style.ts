import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
    container: {
        height: 100,
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        borderTopWidth: 1,
        borderTopColor: '#f1edea'
    },
    top: {
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingHorizontal: 10
    },
    top_amount: {
        padding: 5,
        borderColor: 'gray',
        borderWidth: 1,
        marginRight: 20,
        borderRadius: 5
    },
    top_money: {
        color: '#f60',
        marginRight: 10
    },
    bottom: {
        backgroundColor: 'pink',
        flex: 1,
        flexDirection: 'row'
    },
    bottom_payment: {
        flex: 1,
        backgroundColor: '#007bff'
    },
    bottom_btn: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center', 
        height: '100%'
    },
    bottom_inform: {
        flex: 1,
        backgroundColor: '#28a745'
    }
})
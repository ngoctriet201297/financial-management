import React, { useCallback } from 'react';
import { Header, Left, Body, Right, Text } from 'native-base';
import { FontAwesome5 } from '@expo/vector-icons';
import { Styles } from './style';
import { useNavigation } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Alert } from 'react-native';

type Props = {
    cancelOrder: () => any;
}
export function BillHeader({cancelOrder}: Props) {
    const navigation = useNavigation();

    const deleteOrder = useCallback(() => {
        Alert.alert('Bạn có chắc xóa đơn hàng?', '',
            [{
                text: "Hủy bỏ",
                style: "cancel"
            },
            { text: "Đồng ý", onPress: () => cancelOrder() }]);
    },[cancelOrder]) 

    return (
        <Header style={Styles.container}>
            <Left>
                <TouchableOpacity onPress={deleteOrder}>
                    <FontAwesome5 name="times" size={30} color="#fff" />
                </TouchableOpacity>
            </Left>
            <Body>
                <Text style={Styles.title}>Hóa đơn</Text>
            </Body>
            <Right>
                <TouchableOpacity onPress={() => navigation.navigate('Menu')}>
                    <FontAwesome5 name="plus" size={30} color="#fff" />
                </TouchableOpacity>
            </Right>
        </Header>
    );
}
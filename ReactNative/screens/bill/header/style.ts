import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
    container: {
        backgroundColor: '#f60',
        color: '#fff'
    },
    title: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '600'
    }
})
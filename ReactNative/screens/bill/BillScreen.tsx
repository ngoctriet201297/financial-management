import React, { useEffect } from 'react';
import { Text, View } from 'react-native';
import { Container, Header, Content, Footer, List, ListItem, Left, Button, Icon, Body, Right, Switch } from 'native-base';
import { BillHeader } from './header/bill-header';
import { Styles } from './style';
import { FontAwesome } from '@expo/vector-icons';
import { BillFooter } from './footer/BillFooter';
import { BillItem } from './item/BillItem';
import { Order } from '../../models/order/order';
import { CreateMenu } from '../../models/order/create-order';
import { Promotion } from '../../models/Promotion/Promotion';
import { checkCustomerInvalid } from '../../utility/work-flow';
import { Customer } from '../../models/customer/customer';


type Props = {
    order: Order,
    cancelOrder: () => any,
    updateItem: (input: CreateMenu) => any,
    promotions: Promotion[],
    customer: Customer
}

export function BillScreen({order,cancelOrder,updateItem,promotions,customer}: Props) {

    useEffect(() => {
        checkCustomerInvalid(customer);
    }, [customer])
  

    return (<Container>
        <BillHeader cancelOrder={cancelOrder}/>
        <View style={Styles.subHeader}>
            <Text style={Styles.sub_header_text}>{order?.customersName}</Text>
            <FontAwesome name="chevron-right" size={18} color="gray" />
        </View>
        <View style={{ flex: 1, backgroundColor: '#f1edea' }}>
            <Content>
                {order?.orderDetails?.map((orderDetail) => 
                 <BillItem promotions={promotions} updateItem={updateItem} key={orderDetail.id} orderDetail={orderDetail} />)}
            </Content>
            <BillFooter order={order}/>
        </View>
    </Container>)
}
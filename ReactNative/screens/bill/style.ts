import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
    subHeader: {
        height: 40,
        width: '100%',
        fontSize: 18,
        padding: 10,
        backgroundColor: '#f1edea',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    sub_header_text: {
        fontWeight: '500',
        paddingRight: 10
    },
    body: {
        flex: 1,
        flexDirection: 'column'
    }
});
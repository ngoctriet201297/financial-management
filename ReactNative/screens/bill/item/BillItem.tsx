import React, { useState, useCallback } from 'react';
import { ListItem, Left, Right, Text, View, Picker, Icon } from 'native-base';
import { Styles } from './style';
import { OrderDetail } from '../../../models/order/order-detail';
import { CreateMenu } from '../../../models/order/create-order';
import { Promotion } from '../../../models/Promotion/Promotion';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { FontAwesome } from '@expo/vector-icons';
import { FormatNumber } from '../../../utility/format-numbet';

type Props = {
    orderDetail: any,
    updateItem: (input: CreateMenu) => any,
    promotions: Promotion[]
}
export function BillItem({ orderDetail, promotions, updateItem }: Props) {

    const [localPromotion, setLocalPromotion] = useState(0);
    const [isShow, setIsShow] = useState(false);

    const setPromotionForOrder = useCallback((promotionId: number) => {

        let promotion = promotions.find(x => x.id === promotionId);
        setLocalPromotion(promotionId);
        updateItem({
            ...orderDetail,
            discountPer: promotion?.percentage || 0,
            discountAmout: promotion?.amount || 0,
            promotionCode: promotion?.promotionCode || '',
            promotionId: promotion?.id || null,
            promotionName: promotion?.promotionName || ''
        })
    },[promotions,orderDetail]) 

    return (
        <View>
            <TouchableOpacity style={Styles.container} onPress={() => setIsShow(!isShow)}>
                <View>
                    <Text style={Styles.name}>
                        <Text>{orderDetail.listProductName}</Text>
                        <Text style={Styles.unit}> ({orderDetail.unitsName})</Text>
                    </Text>
                    <Text>
                        <Text>{FormatNumber.currency(orderDetail.menuAmountSalling)} </Text>
                        <Text style={Styles.multi} >x</Text>
                    </Text>
                </View>
                <View>
                    <Text>{orderDetail.itemQuantity}</Text>
                </View>
            </TouchableOpacity>
            {isShow ? <View style={Styles.containerWrapper}>
                <View style={Styles.secondContainer}>
                    <View style={Styles.quantityContainer}>
                        <View>
                            <Text>Số lượng</Text>
                        </View>
                        <View style={Styles.quantityBox}>
                            <TouchableOpacity onPress={() => {
                                updateItem({
                                    ...orderDetail,
                                    itemQuantity: (orderDetail?.itemQuantity || 1) - 1
                                })
                            }}>
                                <FontAwesome name="minus" size={20} color='gray' style={{
                                    marginBottom: 5
                                }} />
                            </TouchableOpacity>
                            <Text style={Styles.textInputQuantity}>{orderDetail?.itemQuantity || 0}</Text>
                            <TouchableOpacity onPress={() => {
                                updateItem({
                                    ...orderDetail,
                                    itemQuantity: (orderDetail?.itemQuantity || 1) + 1
                                })
                            }}>
                                <FontAwesome name="plus" size={20} color='gray' style={{
                                    marginBottom: 5
                                }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={Styles.notifyContainer}>
                        <Text>Chọn khuyến mãi</Text>
                        <Picker
                            note
                            iosIcon={<Icon name="arrow-down" />}
                            mode="dropdown"
                            placeholder="Chọn khuyến mãi..."
                            style={Styles.promotionPicker}
                            selectedValue={localPromotion}
                            onValueChange={setPromotionForOrder}>
                            <Picker.Item label="Chọn khuyến mãi" value="0" />
                            {promotions?.map(item => <Picker.Item key={item.id} label={item.promotionName} value={item.id} />)}
                        </Picker>
                    </View>
                </View>
                <View style={Styles.noteContainer}>
                    <Text>Chú thích</Text>
                    <TextInput style={Styles.noteContainerText} onChangeText={(value)=>updateItem({
                        ...orderDetail,
                        orderDetailNote: value
                    })}
                     value={orderDetail?.orderDetailNote ?? ''} placeholder="Chú thích..." />
                </View>
            </View>
                : null}

        </View>
    );
}
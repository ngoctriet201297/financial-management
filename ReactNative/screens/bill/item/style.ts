import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        alignItems: 'center',
        paddingVertical: 20,
        borderBottomColor: '#f1edea',
        borderBottomWidth: 1
    },
    containerWrapper: {
        flex: 1,
        flexDirection: 'column',
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#fff'
    },
    secondContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    quantityContainer: {
        flexDirection: 'column',
        marginRight: 15,
        minWidth: 100
    },
    quantityBox: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 15,
        justifyContent: 'space-between'
    },
    notifyContainer: {
        flexDirection: 'column',
        flexGrow: 1,
    },
    name: {
        marginBottom: 10,
        fontWeight: '600',
    },
    unit: {
        color: 'gray',
        fontWeight: 'normal',
    },
    multi: {
        color: '#f60',
        fontSize: 18
    },
    textInputQuantity: {
        paddingRight: 10,
        marginLeft: 10
    },
    noteContainer: {
        flex: 1
    },
    noteContainerText: {
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        borderColor: 'gray'
    },
    promotionPicker: {
        flex: 1,
        backgroundColor: '#fff'
    }
});
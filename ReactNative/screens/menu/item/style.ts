import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
    left: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    text: {
        width: 80,
        height: 30,
        textAlign: 'center',
        backgroundColor: '#ccc',
        paddingTop: 5,
        borderRadius: 10
    }
})
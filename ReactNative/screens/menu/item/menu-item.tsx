import React from 'react';
import { } from 'react-native';
import { ListItem, Body, Left, Right, Thumbnail, Text } from 'native-base';
import { TouchableOpacity, TextInput } from 'react-native-gesture-handler';
import { FontAwesome } from '@expo/vector-icons';
import { Styles } from './style';
import { Menu } from '../../../models/menu/menu';
import { CreateMenu } from '../../../models/order/create-order';
import { FormatNumber } from '../../../utility/format-numbet';
type Props = {
    item: CreateMenu,
    update: (input: CreateMenu) => any
}
export function MenuItem({ item, update }: Props) {

    return (<ListItem thumbnail>
        <Left>
            <Thumbnail square source={{ uri: 'https://assets.grab.com/wp-content/uploads/sites/11/2020/05/22030833/foodholicvn_97205463_895809214216336_5775493243925186613_n-e1590088155253.jpg' }} />
        </Left>
        <Body>
            <Text>{item?.menuPriceName}</Text>
            <Text>{FormatNumber.currency(item?.menuAmountSalling || 0)}/{item?.unitsName}</Text>
        </Body>
        <Right style={Styles.left}>
            <TouchableOpacity onPress={() => {
                update({
                    ...item,
                    itemQuantity: (item.itemQuantity || 1) - 1
                })
            }}>
                <FontAwesome name="minus" size={20} color='gray' style={{
                    marginBottom: 5
                }} />

            </TouchableOpacity>
            <Text style={Styles.text}>
                {item.itemQuantity || 0}
            </Text>
            <TouchableOpacity onPress={() => {
                update({
                    ...item,
                    itemQuantity: (item.itemQuantity || 0) + 1
                })
            }}>
                <FontAwesome name="plus" size={20} color='gray' style={{
                    marginBottom: 5
                }} />
            </TouchableOpacity>
        </Right>
    </ListItem>)
}
import React, { useState, useEffect, useCallback } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { View, Text, Alert } from 'react-native';
import { Container, Footer, Left, Right } from 'native-base';
import { Styles } from './style';
import { MenuTab } from './tab/menu-tab';
import { TouchableOpacity, TextInput } from 'react-native-gesture-handler';
import { GlobalStyles } from '../../shared/styles/global-style';
import { FontAwesome, Ionicons, FontAwesome5 } from '@expo/vector-icons';
import { DismissKeyboardHOC } from '../../shared/higher-components';
import IOrderState from '../../redux/states/order/order-state';
import { CreateGroupMenu } from '../../models/order/create-group-menu';
import { CreateMenu } from '../../models/order/create-order';
import { Order } from '../../models/order/order';
import { checkCustomerInvalid } from '../../utility/work-flow';
import { Customer } from '../../models/customer/customer';


type Props = {
    groupMenus: CreateGroupMenu[],
    setFilter: (filter: string) => any,
    filter: string,
    updateItem: (input: CreateMenu) => any,
    cancelOrder: () => any,
    order: Order,
    resetOrder: () => any,
    customer: Customer
}
export function MenuScreen({
    groupMenus, setFilter, filter, updateItem,
    cancelOrder, order, resetOrder, customer }: Props) {

    const navigation = useNavigation();
    const route = useRoute();
    const [isShownSearch, setIsShownSearch] = useState(false);
    const deleteOrder = useCallback(() => {
        Alert.alert('Xóa đơn hàng', 'Bạn có chắc xóa đơn hàng',
            [{
                text: "Hủy bỏ",
                style: "cancel"
            },
            { text: "Đồng ý", onPress: () => cancelOrder() }]);
    }, [cancelOrder])

    const refreshOrder = useCallback(() => {
        Alert.alert('Chọn lại đơn hàng?', '',
            [{
                text: "Hủy bỏ",
                style: "cancel"
            },
            { text: "Đồng ý", onPress: () => resetOrder() }]);
    }, [resetOrder])

    const pressSearch = useCallback( () => {
        setIsShownSearch(true);
    }, [setIsShownSearch])

    useEffect(() => {
        checkCustomerInvalid(customer);
    }, [customer]);

    return (
        <DismissKeyboardHOC>
            <Container>
                <View style={[Styles.header_container, GlobalStyles.bg_main]}>
                    <TouchableOpacity onPress={() => {
                        if (isShownSearch) {
                            setFilter('');
                        } else {
                            deleteOrder();
                        }
                    }}>
                        <FontAwesome5 size={25} name="times" color="white" />
                    </TouchableOpacity>
                    <View style={Styles.input_search_container}>
                        {
                            isShownSearch ?
                                <TextInput autoFocus={true} style={Styles.input_search}
                                    value={filter ?? ''}
                                    placeholder="Tìm kiếm"
                                    placeholderTextColor="white"
                                    onChangeText={(value) => setFilter(value)}
                                    onBlur={() => setIsShownSearch(false)}></TextInput> :
                                <Text style={
                                    [GlobalStyles.text_size_20, GlobalStyles.color_white]
                                }>Chọn món vào đơn</Text>
                        }
                    </View>
                    <TouchableOpacity onPress={pressSearch}>
                        <FontAwesome5 name="search" size={20} color="white" />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                    <MenuTab updateItem={updateItem} menuGroups={groupMenus || []} />
                </View>
                <Footer style={Styles.footer}>
                    <Left style={Styles.footer_left}>
                        <TouchableOpacity style={Styles.footer_right} onPress={refreshOrder}>

                            <FontAwesome name="refresh" size={20} color="white" />
                            <Text style={Styles.footer_left_tetx}>Chọn lại</Text>
                        </TouchableOpacity>

                    </Left>

                    <Right>
                        <TouchableOpacity onPress={() => navigation.navigate('Bill')}>
                            <View style={Styles.footer_right}>
                                <FontAwesome name="chevron-right" size={20} color="white" />
                                <Text style={Styles.footer_right_text}>Xong</Text>
                                <FontAwesome name="credit-card" size={20} color="white" />
                            </View>
                        </TouchableOpacity>
                    </Right>

                </Footer>
            </Container>
        </DismissKeyboardHOC >);
}
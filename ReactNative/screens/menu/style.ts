import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
    footer: {
        backgroundColor: '#f60',
        paddingHorizontal: 10,
        color: '#fff'
    },
    footer_left: {
        flex: 1,
        flexDirection: 'row'
    },
    footer_left_tetx: {
        color: '#fff',
        paddingLeft: 5,
        paddingRight:5,
        fontSize: 18
    },
    footer_right: {
        flex: 1,
        flexDirection: 'row-reverse',
        alignItems: 'center'
    },
    footer_right_text: {
        paddingHorizontal: 10,
        color: '#fff',
        fontSize: 18
    },
    tab_text: {
        color: 'white',
        fontWeight: '600'
    },
    tab: {
        backgroundColor: '#ab4a09f2'
    },
    tab_active: {
        backgroundColor: '#ab4a09f2'
    },
    tab_active_text: {
        color: '#ffffff'
    },
    tabBarUnderlineStyle: {
        backgroundColor: '#f38c48'
    },
    subHeader: {
        height: 40,
        width: '100%',
        fontSize: 18,
        padding: 10,
        backgroundColor: '#f1edea'
    },
    sub_header_text: {
        fontWeight: '500'
    },
    header_container: {
        paddingTop: 15,
        height: 70,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    input_search: {
        borderColor: '#fff',
        color: '#fff',
        paddingHorizontal: 10,
        height: 40,
        borderWidth: 1,
        borderRadius: 10,
        width: '100%',
        textAlign: 'right'
    },
    input_search_container: {
        flexGrow: 1,
        paddingHorizontal: 15,
        alignItems: 'center'
    }
});
import React from 'react';
import { } from 'react-native';
import { Tab, View, Text, ScrollableTab, Tabs, Content } from 'native-base';
import { Styles } from '../style';
import { GroupMenu } from '../../../models/menu/group-menu';
import { MenuItem } from '../item/menu-item';
import { CreateMenu } from '../../../models/order/create-order';

type Props = {
    menuGroups: GroupMenu[],
    updateItem: (input: CreateMenu) => any
}
export function MenuTab({ menuGroups, updateItem }: Props) {

    return (
        <Tabs renderTabBar={() => <ScrollableTab />}
            tabBarUnderlineStyle={Styles.tabBarUnderlineStyle} >
            {
                <Tab heading={'Tất cả'} textStyle={Styles.tab_text} tabStyle={Styles.tab}
                    activeTabStyle={[Styles.tab_active]}
                    activeTextStyle={Styles.tab_active_text}>
                    <View style={Styles.subHeader}>
                        <Text>Tất cả</Text>
                    </View>
                    <Content>
                        {menuGroups?.map(groupMenu =>groupMenu.menus?.map(menu =>
                            <MenuItem update={updateItem} key={menu.id} item={menu}></MenuItem>))}
                    </Content>
                </Tab>
            }
            {
                menuGroups?.map(groupMenu => (
                    <Tab key={groupMenu.id} heading={groupMenu.name || ''} textStyle={Styles.tab_text} tabStyle={Styles.tab}
                        activeTabStyle={[Styles.tab_active]}
                        activeTextStyle={Styles.tab_active_text}>
                        <View style={Styles.subHeader}>
                            <Text>{groupMenu.name}</Text>
                        </View>
                        <Content>
                            {groupMenu.menus?.map(menu =>
                                <MenuItem update={updateItem} key={menu.id} item={menu}></MenuItem>)}
                        </Content>
                    </Tab>))
            }
        </Tabs>
    )
}
import { StyleSheet } from 'react-native';

export const Style = StyleSheet.create({
    tab_text: {
        color: 'white',
        fontWeight: '600'
    },
    tab: {
        backgroundColor: '#ab4a09f2'
    },
    tab_active: {
        backgroundColor: '#ab4a09f2'
    },
    tab_active_text: {
        color: '#ffffff'
    },
    tabBarUnderlineStyle: {
        backgroundColor: '#f38c48'
    },
    subHeader: {
        height: 40,
        width: '100%',
        fontSize: 18,
        padding: 10,
        backgroundColor: '#f1edea'
    },
    sub_header_text: {
        fontWeight: '500'
    }
})
import React from 'react';
import {
    View, Text
} from 'react-native';
import { Container } from 'native-base'
import Styles from './style';
import * as Animatable from 'react-native-animatable';

export function IntroduceScreen() {

    return (
        <Container style={Styles.container}>
            <View style={Styles.header}>
                <Text style={Styles.text_header}>Welcome!</Text>
            </View>
            <Animatable.View
                animation="fadeInUpBig"
                style={Styles.footer}>
                    <Text style={Styles.text_footer}>Đang tải dữ liệu...</Text>
            </Animatable.View>
        </Container>);
}
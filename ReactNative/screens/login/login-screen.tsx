import React, { useState } from 'react';
import {
    View, Text, TextInput, TouchableOpacity,
    KeyboardAvoidingView
}
    from 'react-native';
import { Container, Content } from 'native-base'
import Styles from './style';
import { FontAwesome, Feather } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import * as Animatable from 'react-native-animatable';
import { DismissKeyboardHOC } from '../../shared/higher-components';
import { UserLoginInput } from '../../models/user-login/user-login-input';
import { useNavigation } from '@react-navigation/native';

type Props = {
    login: (input: UserLoginInput) => void,
    logOut: () => any
}
export default function LoginScreen({logOut, login}: Props) {

    const navigation = useNavigation();
    const [userLogin, setUserLogin] = useState({
        username: 'dev',
        password: 'Sdhris@123',
        secureTextEntry: true
    })

    const updateSecureTextEntry = () => {
        setUserLogin({
            ...userLogin,
            secureTextEntry: !userLogin.secureTextEntry
        })
    }

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            logOut();
        });

        return unsubscribe;
    }, [navigation]);

    return (
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
            <DismissKeyboardHOC>
                <Container style={Styles.container}>
                    <View style={Styles.header}>
                        <Text style={Styles.text_header}>Chào mừng!</Text>
                    </View>
                    <Animatable.View
                        animation="fadeInUpBig"
                        style={Styles.footer}>
                        <Text style={Styles.text_footer}>Tài khoản</Text>
                        <View style={Styles.action}>
                            <FontAwesome name='user-o' color="#05375a" size={20} />
                            <TextInput style={Styles.textInput} value={userLogin.username} onChangeText={(value) => setUserLogin({ ...userLogin, username: value })}
                                placeholder="Tài khoản" autoCapitalize="none" />
                        </View>
                        <Text style={[Styles.text_footer, { marginTop: 30 }]}>Mật khẩu</Text>
                        <View style={Styles.action}>
                            <FontAwesome name='lock' color="#05375a" size={20} />
                            <TextInput style={Styles.textInput} placeholder="Mật khẩu của bạn"
                                value={userLogin.password} onChangeText={(value) => setUserLogin({ ...userLogin, password: value })}
                                autoCapitalize="none" secureTextEntry={userLogin.secureTextEntry} />
                            <TouchableOpacity onPress={() => updateSecureTextEntry()}>
                                {
                                    userLogin.secureTextEntry ? <FontAwesome name='eye-slash' color="#05375a" size={20} /> :
                                        <FontAwesome name='eye' color="#05375a" size={20} />
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={Styles.button}>
                            <TouchableOpacity onPress={() => login({
                                username: userLogin?.username,
                                password: userLogin?.password
                            })} activeOpacity={0.8}>
                                <LinearGradient colors={['#f60', '#d46318']} style={Styles.signIn}>
                                    <Text style={[Styles.textSign, {
                                        color: '#fff'
                                    }]}>đăng nhập</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </Animatable.View>
                </Container>
            </DismissKeyboardHOC>
        </KeyboardAvoidingView>

    )
}
import React, { useState, useEffect } from 'react';
import { Text, View, TextInput, } from 'react-native';
import { Container, Content, Footer, ListItem, Left, Right, Button, Picker, Icon } from 'native-base';
import { FontAwesome } from '@expo/vector-icons';
import { Styles } from './style';
import { GlobalStyles } from '../../shared/styles/global-style';
import { Order } from '../../models/order/order';
import { FormatNumber } from '../../utility/format-numbet';
import { OrderType } from '../../models/OrderType/OrderType';
import { HrisNotify } from '../../utility/hris-notify';
import { checkCustomerInvalid } from '../../utility/work-flow';
import { Customer } from '../../models/customer/customer';

type Props = {
    order: Order,
    placeOrder: (input: Order) => any,
    orderTypes: OrderType[],
    customer: Customer
}
export function PaymentScreen({ order, placeOrder, orderTypes, customer }: Props) {

    const [amountPaidedCustomer, setAmountPaidedCustomer] = useState(0);
    const [amountPaidedCustomerText, setAmountPaidedCustomerText] = useState('0');
    const [orderTypeId, setOrderType] = useState(orderTypes?.length && orderTypes[0].id);
    const [note, setNote] = useState('')

    let localQuantity = 0;
    let localAmount = 0;
    let localAmountDiscount = 0;

    order?.orderDetails?.forEach(orderDetail => {
        localQuantity += (orderDetail.itemQuantity || 0);
        localAmount += (orderDetail.itemTotalAmount || 0);
        localAmountDiscount += (orderDetail.itemDiscountTotal || 0);
    });
    let exchangecustomersMoney = (amountPaidedCustomer || 0) - ((localAmount || 0) - (localAmountDiscount || 0));

    const submitOrder = () => {
        let localOrderType = orderTypes?.find(item => item.id === orderTypeId);
        if (!localOrderType) {
            HrisNotify.warn('Vui lòng chọn loại khách hàng');
            return;
        }
        placeOrder({
            ...order,
            customersMoney: amountPaidedCustomer || 0,
            exchangecustomersMoney,
            totalAmount: (localAmount || 0) - (localAmountDiscount || 0),
            totalDiscount: localAmountDiscount || 0,
            totalPrice: localAmount || 0,
            totalQuantity: localQuantity || 0,
            orderNote: note,
            orderTypeCode: localOrderType?.orderTypeCode,
            orderTypeId: (localOrderType?.id as string) || undefined,
            orderTypeName: localOrderType?.orderTypeName
        })
    }

    useEffect(() => {
        checkCustomerInvalid(customer);
    }, [customer]);


    return (<Container>
        <Content style={[GlobalStyles.bg_gray]}>
            <View style={[Styles.payment_method]}>
                <Text style={[Styles.text]}>Phương thức thanh toán</Text>
                <View style={[Styles.payment_method_money]}>
                    <Text style={[Styles.text, Styles.payment_method_text]}>Tiền mặt</Text>
                    <FontAwesome size={15} color="gray" name="chevron-right" />
                </View>
            </View>
            <View style={[Styles.payment_method]}>
                <Text style={[Styles.text]}>Loại khách</Text>
                <Picker
                    note
                    iosIcon={<Icon name="arrow-down" />}
                    mode="dropdown"
                    placeholder="Chọn loại khách hàng..."
                    style={Styles.promotionPicker}
                    selectedValue={orderTypeId}
                    onValueChange={setOrderType}>
                    {orderTypes?.map(item => <Picker.Item key={item.id} label={item.orderTypeName} value={item.id} />)}
                </Picker>
            </View>

            <View style={[Styles.payment_method, GlobalStyles.margin_top_25]}>
                <View style={[GlobalStyles.flex_direction_row]}>
                    <Text style={[Styles.text]}>Tổng tiền hàng</Text>
                    <Text style={[Styles.amount_item]}>{localQuantity}</Text>
                </View>
                <Text style={[Styles.text]}>{FormatNumber.currency(localAmount)}</Text>
            </View>

            <View style={[Styles.payment_method]}>
                <View >
                    <Text style={[Styles.text]}>Giảm giá</Text>
                </View>
                <View style={Styles.discount_right}>
                    <Text style={[Styles.text]}>{FormatNumber.currency(localAmountDiscount)}</Text>
                </View>
            </View>

            <View style={[Styles.payment_method]}>
                <View>
                    <Text style={[Styles.text]}>Khách cần trả</Text>
                </View>
                <View>
                    <Text style={[Styles.text, GlobalStyles.color_red,
                    GlobalStyles.font_weight_500]}>{FormatNumber.currency((localAmount || 0) - (localAmountDiscount || 0))}</Text>
                </View>
            </View>

            <View style={[Styles.payment_method]}>
                <View>
                    <Text style={[Styles.text]}>Khách thanh toán</Text>
                </View>
                <View>
                    <TextInput style={[Styles.discount_input, GlobalStyles.bg_gray]}
                        value={amountPaidedCustomerText || ''} onChangeText={(text) => {
                            const value = text.replace(/\D/g, '');
                            if (value) {
                                const num = parseInt(value, 10);
                                setAmountPaidedCustomerText(FormatNumber.currencyInput(num));
                                setAmountPaidedCustomer(num);
                            } else {
                                setAmountPaidedCustomerText(FormatNumber.currencyInput(0));
                                setAmountPaidedCustomer(0);
                            }
                            setAmountPaidedCustomer(parseInt(text) || 0);
                        }} ></TextInput>
                </View>
            </View>

            <View style={[Styles.payment_method, GlobalStyles.margin_top_25]}>
                <View>
                    <Text style={[Styles.text]}>Tiền thừa trả khách</Text>
                </View>
                <View>
                    <Text style={[Styles.text]}>
                        {FormatNumber.currency(exchangecustomersMoney)}</Text>
                </View>
            </View>
            <View style={[Styles.payment_method]}>
                <View style={Styles.noteContainer}>
                    <Text>Chú thích</Text>
                    <TextInput style={Styles.noteContainerText} onChangeText={setNote}
                        value={note ?? ''} placeholder="Chú thích..." />
                </View>
            </View>

        </Content>
        <Footer>
            <Button style={Styles.footer} onPress={submitOrder}>
                <Text style={Styles.footer_text}>Hoàn thành</Text>
            </Button>
        </Footer>
    </Container>)
}
import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
    footer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f60',
        height: '100%',
        borderRadius: 0
    },
    footer_text: {
        fontSize: 20,
        color: '#fff'
    },
    payment_method: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomColor: '#ddd',
        borderBottomWidth: 1
    },
    payment_method_money: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    payment_method_text: {
        marginRight: 10
    },
    text: {
        fontSize: 17
    },
    amount_item: {
        padding: 5,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 10,
        borderRadius: 5,
        marginTop: -5
    },
    discount_btn: {
        flexDirection: 'row'
    },
    discount_btn_money: {
        backgroundColor: '#f60',
        padding: 10,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    discount_btn_percent:{
        borderColor: '#f60',
        borderWidth: 1,
        padding: 9,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    discount_right: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    discount_input: {
        borderColor: '#ddd',
        borderWidth: 1,
        borderRadius: 5,
        fontSize: 18,
        minWidth: 140,
        height: 40,
        marginLeft: 5,
        textAlign: 'right',
        paddingHorizontal: 10
    },
    amount_customer_pay: {
        padding: 10,
        borderRadius: 5,
        backgroundColor: '#ddd',
        flexDirection: 'row-reverse',
        width: 140
    },
    promotionPicker: {
        flex: 1,
        backgroundColor: '#fff'
    },
    noteContainer: {
        flex: 1
    },
    noteContainerText: {
        marginTop: 10,
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        borderColor: 'gray'
    },
})
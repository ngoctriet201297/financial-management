import React from 'react';
import { Header, Left, Body, Right, Text } from 'native-base';
import { FontAwesome5 } from '@expo/vector-icons';
import { Styles } from './style';
import { useNavigation, DrawerActions } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export function StoreHeader() {
    const navigation = useNavigation();

    return (
        <Header style={Styles.container}>
            <Left>
                <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
                    <FontAwesome5 name="bars" size={25} color="#fff" />
                </TouchableOpacity>
            </Left>
            <Body>
                <Text style={Styles.title}>Cửa hàng</Text>
            </Body>
            <Right>
            </Right>
        </Header>
    );
}
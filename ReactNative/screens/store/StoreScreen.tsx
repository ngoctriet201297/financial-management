import React from 'react';
import { View, Text } from 'react-native';
import { Container, Content, Left, Body, Header, Right } from 'native-base';
import { StoreHeader } from './header/StoreHeader';
export function StoreScreen() {
    return (<Container>
        <StoreHeader/>
        <Content>
            <Text>Thông tin cửa hàng</Text>
        </Content>
    </Container>);
}
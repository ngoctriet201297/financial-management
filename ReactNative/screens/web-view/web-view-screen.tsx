import { WebView } from 'react-native-webview';
import React, { useEffect, useState, useCallback } from 'react';
import { IAppState } from '../../redux/states';
import { connect } from 'react-redux';
import { Order } from '../../models/order/order';
import { PrinterService } from '../../service/printer-bill/printer-service';
import { Container, Header, Left, Body, Right, Text } from 'native-base';
import { TouchableOpacity } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import { navigate } from '../../service/navigation/navigation-service';
import { Styles } from './styles';
import { useNavigation } from '@react-navigation/native';
import { OrderAction } from '../../redux/actions/order/order-action';
import { checkCustomerInvalid } from '../../utility/work-flow';
import { Customer } from '../../models/customer/customer';

type Props = {
    order?: Order,
    clearOrder: () => any,
    customer?: Customer
}
function WebViewScreen({ order, clearOrder, customer }: Props) {

    const navigation = useNavigation();
    const [template, setTemplate] = useState(PrinterService.setTemplate1(order || new Order()));
    const [isTemplate1, setIsTemplate1] = useState(true);

    useEffect(() => {
        if (order) {
            setTemplate(PrinterService.setTemplate1(order));
            setIsTemplate1(true);
        }

    }, [order])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            clearOrder();
        });
        return unsubscribe;
    }, [navigation]);


    const pressPrint = useCallback(() => {
        if (isTemplate1 && order) {
            setTemplate(PrinterService.setTemplate2(order));
            setIsTemplate1(false);
        }
    },[isTemplate1, order]);

    useEffect(() => {
        checkCustomerInvalid(customer || new Customer());
    }, [customer]);

    return (<Container>
        <Header style={Styles.container}>
            <Left>
                <TouchableOpacity onPress={() => navigate('Table')}>
                    <FontAwesome5 name="chevron-left" size={30} color="#fff" />
                </TouchableOpacity>
            </Left>
            <Body>
                <Text style={Styles.title}>In hóa đơn</Text>
            </Body>
            <Right>
                <TouchableOpacity onPress={pressPrint}>
                    <FontAwesome5 name="print" size={30} color="#fff" />
                </TouchableOpacity>
            </Right>
        </Header>
        <WebView
            source={{
                html: template || '',
            }} />
    </Container>)

}

const mapStateToProps = ({ order, customer }: IAppState) => {
    return {
        order: order?.order,
        customer: customer?.choosenCustomer
    }
}

const mapDispatchToState = (dispatch: any) => {
    return {
        clearOrder: () => dispatch(OrderAction.clear())
    }
}

export default connect(mapStateToProps, mapDispatchToState)(WebViewScreen);
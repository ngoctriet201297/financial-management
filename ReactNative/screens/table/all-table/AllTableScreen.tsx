import React from 'react';
import { View, Text } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Container, Content } from 'native-base';
import { TableItem } from '../item/table-item';
import { GlobalStyles } from '../../../shared/styles/global-style';
import ICustomerState from '../../../redux/states/customer/customer-state';
import { Customer } from '../../../models/customer/customer';

type Props = {
    table: ICustomerState,
    createOrder: (customer: Customer) => any
}
export default function AllTableScreen({table, createOrder}: Props) {

    const navigation = useNavigation();
    const route = useRoute();

    return (<Container>
        <Content style={GlobalStyles.bg_gray}>
            <View style={{ flex: 1, flexWrap: 'wrap', flexDirection: 'row' }}>     
            {table?.customers?.map((item) => 
                <TableItem createOrder={() =>createOrder(item)} customer={item} key={item?.id} /> )}
            </View>
        </Content>
    </Container>);
}
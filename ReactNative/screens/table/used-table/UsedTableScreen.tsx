import React from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { View, Text, TouchableOpacity } from 'react-native';
import { Container, Content } from 'native-base';
import { FontAwesome } from '@expo/vector-icons';
import { Styles } from './style';
import { TableItem } from '../item/table-item';
export default function UsedTableScreen() {
    const navigation = useNavigation();
    const route = useRoute();

    return (<Container>
        <Content style={{ backgroundColor: '#f1edea' }}>
            <View style={{ flex: 1, flexWrap: 'wrap', flexDirection: 'row' }}>
                <TouchableOpacity activeOpacity={0.7}>
                    <View style={Styles.itemContainer}>
                        <FontAwesome name="shopping-basket" style={{
                            color: '#f60',
                            position: 'absolute',
                            top: 10,
                            left: 10
                        }} size={20} />
                        <Text style={Styles.itemText}>Mang về</Text>
                    </View>
                </TouchableOpacity>
                <TableItem name='Bàn 1' />
            </View>
        </Content>
    </Container>);
}
import React from 'react';
import {useRoute, useNavigation} from '@react-navigation/native';
import { View, Text, TouchableOpacity} from 'react-native';
import { Container, Content } from 'native-base';
import { TableItem } from '../item/table-item';
import {FontAwesome} from '@expo/vector-icons';
import { Styles } from './style';

export default function EmptyTableScreen(){
    const route = useRoute();
    const navigation = useNavigation();

    return (<Container>
        <Content style={{ backgroundColor: '#f1edea' }}>
            <View style={{ flex: 1, flexWrap: 'wrap', flexDirection: 'row' }}>
                <TouchableOpacity activeOpacity={0.7}>
                    <View style={Styles.itemContainer}>
                        <FontAwesome name="shopping-basket" style={{
                            color: '#f60',
                            position: 'absolute',
                            top: 10,
                            left: 10
                        }} size={20} />
                        <Text style={Styles.itemText}>Mang về</Text>
                    </View>
                </TouchableOpacity>
                <TableItem name='Bàn 1' />
            </View>
        </Content>
    </Container>);
}
import { StyleSheet, Dimensions } from 'react-native';

const ScreenDimenssion = Dimensions.get("screen");
let WidthItem = 0;
switch (true) {
    case ScreenDimenssion.width >= 500:
        WidthItem = (ScreenDimenssion.width - 30) / 3;
        break;
    default:
        WidthItem = (ScreenDimenssion.width - 20) / 2;
        break;

}

export const Styles = StyleSheet.create({
    container: {
        height: 120,
        width: WidthItem,
        justifyContent: 'center',
        alignItems: 'center', 
        backgroundColor: 'white', 
        margin: 5,
        borderRadius: 10, 
        borderColor: 'gray'
    },
    text: {
        fontSize: 17,
        fontWeight: '500'
    }
});
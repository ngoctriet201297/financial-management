import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Styles } from './style';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Customer } from '../../../models/customer/customer';

export function TableItem({customer, createOrder}: Props) {

    const navigation = useNavigation();

    return (<TouchableOpacity onPress={() => createOrder()}>
        <View style={Styles.container}>
            <Text style={Styles.text}>{customer?.customersName}</Text>
        </View></TouchableOpacity>);
}
type Props = {
    customer: Customer;
    createOrder: () => any
}
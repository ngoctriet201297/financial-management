"use strict";
exports.__esModule = true;
exports.TableItem = void 0;
var react_1 = require("react");
var react_native_1 = require("react-native");
var style_1 = require("./style");
var native_1 = require("@react-navigation/native");
function TableItem(_a) {
    var customer = _a.customer, createOrder = _a.createOrder;
    var navigation = native_1.useNavigation();
    return (react_1["default"].createElement(react_native_1.TouchableOpacity, { onPress: function () { return createOrder(); } },
        react_1["default"].createElement(react_native_1.View, { style: style_1.Styles.container },
            react_1["default"].createElement(react_native_1.Text, { style: style_1.Styles.text }, customer === null || customer === void 0 ? void 0 : customer.customersName))));
}
exports.TableItem = TableItem;

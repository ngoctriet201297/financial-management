import React from 'react';
import configStore from './configuration';
import { Provider } from 'react-redux';
import AppStartContainer from './start-app/app-start-container';
import { initAPIInterceptor } from './interceptors/APIInterceptors';
import { Root } from 'native-base';

const store = configStore();
initAPIInterceptor(store)
export default function App() {

  return (
    <Provider store={store}>
      <AppStartContainer></AppStartContainer>
    </Provider>
  );
}
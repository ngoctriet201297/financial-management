import { connect, ConnectedProps } from 'react-redux';
import { IAppState } from '../redux/states';
import { UserLoginAction } from '../redux/actions';
import { UserLogin } from '../models/user-login';

function mapStateToProps(state: IAppState) {
    return {
        token: state.userLogin.token
    };
}

const mapDispatchToProps = {
    setUser: ({token, username}: UserLogin) => UserLoginAction.LoginSuccess({
        token,
        username
    })
}

export const connector = connect(mapStateToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>;

export type AppStartProps = PropsFromRedux;
import React, { useEffect, useState } from 'react';
import { SpinnerContainer } from '../containers/shared';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { LocalStorageHelper } from '../utility/local-storage-helper/local-storage-helper';
import AuthStackNavigator from '../navigators/AuthNavigator';
import { AppStartProps } from './AppStartProp';
import DrawerNavigator from '../navigators/DrawerNavigator';
import { IntroduceScreen } from '../screens/introduce/introduce-screen';
import { Root } from 'native-base';
import { navigationRef } from '../service/navigation/navigation-service';

const Stack = createStackNavigator();

export default function AppStartComponent(props: AppStartProps) {

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        LocalStorageHelper.getUserLogin().then(user => {
            if (user) {
                props.setUser(user);
            }
            setIsLoading(false);
        });
    }, [])

    if (isLoading) {
        return <IntroduceScreen />
    }

    return (
        <>
            <Root>
                <NavigationContainer ref={navigationRef}>
                    <Stack.Navigator screenOptions={{
                        headerShown: false
                    }}>
                        {
                            props.token ?
                                <Stack.Screen name="Drawer" component={DrawerNavigator}></Stack.Screen>
                                :
                                <Stack.Screen name="Auth" component={AuthStackNavigator}></Stack.Screen>
                        }
                    </Stack.Navigator>
                </NavigationContainer>
            </Root>
            <SpinnerContainer />
        </>)

}
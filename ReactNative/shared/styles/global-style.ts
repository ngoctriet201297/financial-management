import { StyleSheet } from 'react-native';

export const GlobalStyles = StyleSheet.create({
    color_white:{
        color: '#fff'
    },
    main_color: {
        color: '#f60'
    },
    bg_success: {
        backgroundColor: '#28a745'
    },
    bg_white: {
        color: '#fff'
    },
    text_size_20: {
        fontSize: 20
    },
    bg_gray: {
        backgroundColor: '#f1edea'
    },
    flex_direction_row: {
        flexDirection: 'row'
    },
    color_red: {
        color: '#dc3545'
    },
    font_weight_500: {
        fontWeight: '500'
    },
    margin_top_25: {
        marginTop: 25
    },
    bg_main: {
        backgroundColor: '#f60'
    }
})
import React from 'react';
import { ActivityIndicator, View } from "react-native";
import styles from './style';
import ILoadingState from '../../../redux/states/loading/loading-state';

type Props = {
    loading: ILoadingState
}
export default function SpinnerComponent(Props?: Props) {
    return Props?.loading?.loading ? (

        <View style={[styles.backdrop,styles.container ]}>
            <View style={{
                ...styles.backdrop,
                opacity: 0.4
            }}>
            </View>
            <ActivityIndicator color="#f60" size="large" />
        </View>) : null
}
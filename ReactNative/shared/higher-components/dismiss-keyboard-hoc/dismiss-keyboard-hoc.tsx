import React from 'react';
import { TouchableNativeFeedback, View, Keyboard } from 'react-native';
export function DismissKeyboardHOC({ children, ...props }: Props) {

    return (<TouchableNativeFeedback accessible={false} onPress={Keyboard.dismiss} >
        {children}
    </TouchableNativeFeedback>)
}
type Props = {
    children: any,
    props?: any
}
import { connect, useDispatch } from 'react-redux';
import { IAppState } from '../../redux/states';
import { StoreScreen } from '../../screens/store/StoreScreen';

function mapStateToProps(state: IAppState){
    return {

    }
}

function mapDispatchToProps(dispatch: any){
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreScreen);
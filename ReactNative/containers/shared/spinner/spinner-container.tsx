import {connect} from 'react-redux';
import {SpinnerComponent} from '../../../shared/components/shared'
import { IAppState } from '../../../redux/states';
function mapStateToProps(state: IAppState){
    return {
        loading: state.loading
    };
}

function mapDispatchToProps(dispatch: any){
    return {
        
    };
}

const SpinnerContainer = connect(mapStateToProps, mapDispatchToProps)(SpinnerComponent);

export default SpinnerContainer;
import {connect} from 'react-redux';
import { IAppState } from '../../redux/states';
import AllTableScreen from '../../screens/table/all-table/AllTableScreen';
import { OrderAction } from '../../redux/actions/order/order-action';
import { GroupMenu } from '../../models/menu/group-menu';
import { Customer } from '../../models/customer/customer';

function mapStateToProps(state: IAppState){
    return {
        table: state.customer,
        groupMenus: state?.menu.groups,
        customers: state.customer?.customers || []
    }
}

function mapDispatchToProps(dispatch: any){
    return {
        createOrder:(data: GroupMenu[], customer: Customer) => {
            return dispatch(OrderAction.createOrder(data,customer));
        }
    }
}
function mergeProps(stateProps: any, dispatchProps: any, ownProps: any){
    return {
        ...ownProps,
        ...stateProps,
        ...dispatchProps,
        createOrder: (customer: Customer) => {
            if(customer){
                return dispatchProps.createOrder(stateProps.groupMenus, customer);
            }
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AllTableScreen);
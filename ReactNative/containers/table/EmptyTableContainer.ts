import {connect} from 'react-redux';
import { IAppState } from '../../redux/states';
import EmptyTableScreen from '../../screens/table/empty-table/EmptyTableScreen';

function mapStateToProps(state: IAppState){
    return {

    }
}

function mapDispatchToProps(dispatch: any){
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EmptyTableScreen);
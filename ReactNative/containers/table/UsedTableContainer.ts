import { connect } from 'react-redux';
import { IAppState } from '../../redux/states';
import UsedTableScreen from '../../screens/table/used-table/UsedTableScreen';

function mapStateToProps(state: IAppState){
    return {

    }
}

function mapDispatchToProps(dispatch: any){
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsedTableScreen);
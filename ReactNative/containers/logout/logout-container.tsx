import {connect} from 'react-redux';
import LoginComponent from '../../screens/login/login-screen';
import { LogoutScreen } from '../../screens/logout/LogoutScreen';
import { UserLoginAction } from '../../redux/actions';

function mapStateToProps(state: any){
    return {

    };
}
function mapDispatchToProps(dispatch: any){
    return {
        logOut: () => dispatch(UserLoginAction.Logout())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LogoutScreen);
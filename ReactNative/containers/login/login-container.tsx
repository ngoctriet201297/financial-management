import {connect} from 'react-redux';
import LoginComponent from '../../screens/login/login-screen';
import { IAppState } from '../../redux/states';
import { UserLoginAction } from '../../redux/actions';
import { UserLoginInput } from '../../models/user-login/user-login-input';

function mapStateToProps(state: IAppState){
    return {
        Props: state.userLogin
    };
}
function mapDispatchToProps(dispatch: any){
    return {
        login:(input: UserLoginInput) => dispatch(UserLoginAction.Login(input)),
        logOut: () => dispatch(UserLoginAction.Logout())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent);
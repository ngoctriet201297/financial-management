import { connect, ConnectedProps } from 'react-redux';
import { IAppState } from '../../redux/states';
import { MenuScreen } from '../../screens/menu/MenuScreen';
import { OrderAction } from '../../redux/actions/order/order-action';
import { CreateGroupMenu } from '../../models/order/create-group-menu';
import { OrderDetail } from '../../models/order/order-detail';
import { CreateMenu } from '../../models/order/create-order';

function mapStateToProps({ order, menu, customer }: IAppState) {
    return {
        groupMenus: order?.groupMenus?.map((groupMenu) => {
            return {
                ...groupMenu,
                menus: groupMenu
                    .menus?.filter(menu => (menu?.menuPriceName?.indexOf(order.filter || '') || 0) >= 0)
            }
        }) || [],
        filter: order?.filter,
        order: {
            ...order.order,
            orderDetails: convertCreateMenuToOrderDetail(order?.groupMenus || [])
        },
        orginalGroupMenus: menu.groups,
        customer: customer?.choosenCustomer
    }
}

function convertCreateMenuToOrderDetail(input: CreateGroupMenu[]): any {
    let result = new Array();
    input?.forEach(groupMenu => {
        let filteredGroupMenu = groupMenu.menus?.filter(menu => (menu?.itemQuantity || 0) > 0)?.map(
            menu => {
                return { ...menu,
                itemTotalAmount: (menu?.itemQuantity || 0) * (menu?.menuAmountSalling || 0)}
            }
        );
        if ((filteredGroupMenu?.length || 0) > 0) {

            result = result.concat(filteredGroupMenu);
        }
    });
    return result;
}

function mapDispatchToProps(dispatch: any, ownProps: any) {
    return {
        setFilter: (filter: string) => dispatch(OrderAction.setFilterOrder(filter)),
        cancelOrder: (order: any) => dispatch(OrderAction.cancelOrder(order)),
        updateItem: (input: CreateMenu) => {
            return dispatch(OrderAction.updateItem(input));
        },
        resetOrder: (input: CreateGroupMenu[]) => dispatch(OrderAction.setDetailOrder(input))
    }
}

function mergeProps(stateProps: any, dispatchProps: any, ownProps: any) {
    return {
        ...ownProps,
        ...stateProps,
        ...dispatchProps,
        cancelOrder: () => dispatchProps.cancelOrder(stateProps.order),
        resetOrder: () => dispatchProps.resetOrder(stateProps.orginalGroupMenus)
    }
}

const connector = connect(mapStateToProps, mapDispatchToProps, mergeProps);

type PropsFromRedux = ConnectedProps<typeof connector>

export type Props = PropsFromRedux;

export default connector(MenuScreen);
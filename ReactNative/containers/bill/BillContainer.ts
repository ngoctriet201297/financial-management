import { connect, ConnectedProps } from 'react-redux';
import { IAppState } from '../../redux/states';
import { BillScreen } from '../../screens/bill/BillScreen';
import { CreateGroupMenu } from '../../models/order/create-group-menu';
import { OrderAction } from '../../redux/actions/order/order-action';
import { CreateMenu } from '../../models/order/create-order';

function mapStateToProps({order, promotion, customer}: IAppState){
    return {
        order: {
            ...order.order,
            orderDetails: convertCreateMenuToOrderDetail(order?.groupMenus || [])
        },
        promotions: promotion?.promotions || [],
        customer: customer?.choosenCustomer
    }
}

function mapDispatchToProps(dispatch: any){
    return {
        cancelOrder: (order: any) => dispatch(OrderAction.cancelOrder(order)),
        updateItem: (input: CreateMenu) => dispatch(OrderAction.updateItem(input))
    }
}

function mergeProps(stateProps: any, dispatchProps: any, ownProps: any){
    return {
        ...ownProps,
        ...stateProps,
        ...dispatchProps,
        cancelOrder: () => dispatchProps.cancelOrder(stateProps.order)
    }
}

function convertCreateMenuToOrderDetail(input: CreateGroupMenu[]): any {
    let result = new Array();
    input?.forEach(groupMenu => {
        let filteredGroupMenu = groupMenu.menus?.filter(menu => (menu?.itemQuantity || 0) > 0)?.map(
            menu => {
                return { ...menu,
                itemTotalAmount: (menu?.itemQuantity || 0) * (menu?.menuAmountSalling || 0)}
            }
        );
        if ((filteredGroupMenu?.length || 0) > 0) {

            result = result.concat(filteredGroupMenu);
        }
    });
    return result;
}

const connector = connect(mapStateToProps, mapDispatchToProps, mergeProps);

type  PropsFromRedux = ConnectedProps<typeof connector>;

export type Props = PropsFromRedux;

export default connector(BillScreen);
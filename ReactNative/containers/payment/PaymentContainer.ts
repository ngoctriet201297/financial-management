import { connect, ConnectedProps } from 'react-redux';
import { IAppState } from '../../redux/states';
import { PaymentScreen } from '../../screens/payment/PaymentScreen';
import { CreateGroupMenu } from '../../models/order/create-group-menu';
import { OrderAction } from '../../redux/actions/order/order-action';
import { Order } from '../../models/order/order';

function mapStateToProps({ order, orderType, customer }: IAppState) {
    return {
        order: {
            ...order.order,
            orderDetails: convertCreateMenuToOrderDetail(order?.groupMenus || [])
        },
        orderTypes: orderType?.orderTypes || [],
        customer: customer?.choosenCustomer
    }
}

function convertCreateMenuToOrderDetail(input: CreateGroupMenu[]): any {
    let result = new Array();
    input?.forEach(groupMenu => {
        let filteredGroupMenu = groupMenu.menus?.filter(menu => (menu?.itemQuantity || 0) > 0)?.map(
            menu => {
                return {
                    ...menu,
                    itemTotalAmount: (menu?.itemQuantity || 0) * (menu?.menuAmountSalling || 0)
                }
            }
        );
        if ((filteredGroupMenu?.length || 0) > 0) {

            result = result.concat(filteredGroupMenu);
        }
    });
    return result;
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        placeOrder: (order: Order) => dispatch(OrderAction.placeOrder(order))
    }
}

function mergeProps(stateProps: any, dispatchProps: any, ownProps: any) {
    return {
        ...ownProps,
        ...stateProps,
        ...dispatchProps
    }
}

const connector = connect(mapStateToProps, mapDispatchToProps, mergeProps);

type PropsFromRedux = ConnectedProps<typeof connector>;

export default connector(PaymentScreen);
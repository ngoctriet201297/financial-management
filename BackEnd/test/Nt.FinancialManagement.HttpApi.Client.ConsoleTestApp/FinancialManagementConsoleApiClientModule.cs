﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace Nt.FinancialManagement
{
    [DependsOn(
        typeof(FinancialManagementHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class FinancialManagementConsoleApiClientModule : AbpModule
    {
        
    }
}

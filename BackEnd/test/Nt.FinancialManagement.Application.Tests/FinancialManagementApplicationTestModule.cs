﻿using Volo.Abp.Modularity;

namespace Nt.FinancialManagement
{
    [DependsOn(
        typeof(FinancialManagementApplicationModule),
        typeof(FinancialManagementDomainTestModule)
        )]
    public class FinancialManagementApplicationTestModule : AbpModule
    {

    }
}

﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace Nt.FinancialManagement.MongoDB
{
    [ConnectionStringName(FinancialManagementDbProperties.ConnectionStringName)]
    public interface IFinancialManagementMongoDbContext : IAbpMongoDbContext
    {
        /* Define mongo collections here. Example:
         * IMongoCollection<Question> Questions { get; }
         */
    }
}

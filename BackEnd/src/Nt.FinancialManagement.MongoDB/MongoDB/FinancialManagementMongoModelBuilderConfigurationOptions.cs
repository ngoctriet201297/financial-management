﻿using JetBrains.Annotations;
using Volo.Abp.MongoDB;

namespace Nt.FinancialManagement.MongoDB
{
    public class FinancialManagementMongoModelBuilderConfigurationOptions : AbpMongoModelBuilderConfigurationOptions
    {
        public FinancialManagementMongoModelBuilderConfigurationOptions(
            [NotNull] string collectionPrefix = "")
            : base(collectionPrefix)
        {
        }
    }
}
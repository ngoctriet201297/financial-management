﻿using System;
using Volo.Abp;
using Volo.Abp.MongoDB;

namespace Nt.FinancialManagement.MongoDB
{
    public static class FinancialManagementMongoDbContextExtensions
    {
        public static void ConfigureFinancialManagement(
            this IMongoModelBuilder builder,
            Action<AbpMongoModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new FinancialManagementMongoModelBuilderConfigurationOptions(
                FinancialManagementDbProperties.DbTablePrefix
            );

            optionsAction?.Invoke(options);
        }
    }
}
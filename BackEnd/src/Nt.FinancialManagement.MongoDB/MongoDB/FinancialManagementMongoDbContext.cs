﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace Nt.FinancialManagement.MongoDB
{
    [ConnectionStringName(FinancialManagementDbProperties.ConnectionStringName)]
    public class FinancialManagementMongoDbContext : AbpMongoDbContext, IFinancialManagementMongoDbContext
    {
        /* Add mongo collections here. Example:
         * public IMongoCollection<Question> Questions => Collection<Question>();
         */

        protected override void CreateModel(IMongoModelBuilder modelBuilder)
        {
            base.CreateModel(modelBuilder);

            modelBuilder.ConfigureFinancialManagement();
        }
    }
}
﻿using System;
using AutoMapper;
using Nt.FinancialManagement.Entities;
using Nt.FinancialManagement.Payments.Dto;

namespace Nt.FinancialManagement.Payments
{
    public class PaymentAutoMapperProfile : Profile
    {
        public PaymentAutoMapperProfile()
        {
            CreateMap<PaymentDto, Payment>();
            CreateMap<Payment, PaymentDto>();
            CreateMap<CreateUpdatePaymentDto, Payment>();
            CreateMap<Payment, GetPaymentForEditDto>();
            CreateMap<Payment, GetPaymentForViewDto>();
        }
    }
}

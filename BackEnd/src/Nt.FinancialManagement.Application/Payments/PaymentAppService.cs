﻿using System;
using Nt.FinancialManagement.Entities;
using Nt.FinancialManagement.Payments.Dto;
using Volo.Abp.Domain.Repositories;

namespace Nt.FinancialManagement.Payments
{
    public class PaymentAppService : AppBaseService<Payment, Guid, CreateUpdatePaymentDto, GetPaymentForViewDto, GetPaymentForEditDto, GetPaymentInputDto>, IPaymentAppService
    {
        public PaymentAppService(IRepository<Payment, Guid> paymentRepository) : base(paymentRepository)
        {
        }
    }
}

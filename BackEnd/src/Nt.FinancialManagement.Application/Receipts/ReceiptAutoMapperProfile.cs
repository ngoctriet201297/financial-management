﻿using System;
using AutoMapper;
using Nt.FinancialManagement.Entities;
using Nt.FinancialManagement.Receipts.Dto;

namespace Nt.FinancialManagement.Receipts
{
    public class ReceiptAutoMapperProfile : Profile
    {
        public ReceiptAutoMapperProfile()
        {
            CreateMap<CreateUpdateReceiptDto, Receipt>();
            CreateMap<Receipt, GetReceiptForEditDto>();
            CreateMap<Receipt, GetReceiptForViewDto>();
            CreateMap<Receipt, ReceiptDto>();
            CreateMap<ReceiptDto, Receipt>();
        }
    }
}

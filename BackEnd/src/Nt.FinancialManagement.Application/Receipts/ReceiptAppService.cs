﻿using System;
using Nt.FinancialManagement.Entities;
using Nt.FinancialManagement.Receipts.Dto;
using Volo.Abp.Domain.Repositories;

namespace Nt.FinancialManagement.Receipts
{
    public class ReceiptAppService : AppBaseService<Receipt, Guid, CreateUpdateReceiptDto, GetReceiptForViewDto, GetReceiptForEditDto, GetReceiptInputDto>, IReceiptAppService
    {

        public ReceiptAppService(IRepository<Receipt, Guid> repository) : base(repository)
        {
        }
    }
}

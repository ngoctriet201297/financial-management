﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using Volo.Abp;

namespace Nt.FinancialManagement
{
    public abstract class AppBaseService<TEntity, TPrimaryKey, TCreateUpdate, TViewOutput, TEditOutput, TGetAllInput> :
       FinancialManagementAppService,
       IAppBaseService<TPrimaryKey, TCreateUpdate, TViewOutput, TEditOutput, TGetAllInput>
       where TPrimaryKey : struct
       where TEntity : Entity<TPrimaryKey>
       where TCreateUpdate : EntityDto<TPrimaryKey?>
       where TViewOutput : EntityDto<TPrimaryKey>
       where TEditOutput : EntityDto<TPrimaryKey?>
       where TGetAllInput : PagedAndSortedResultRequestDto
    {
        protected readonly IRepository<TEntity, TPrimaryKey> Repository;
        protected AppBaseService(IRepository<TEntity, TPrimaryKey> repository)
        {
            Repository = repository;
        }

        #region Filter Function
        protected virtual IQueryable<TEntity> CreateFilteredQuery(TGetAllInput input)
        {
            return Repository;
        }

        protected virtual IQueryable<TEntity> CreateFilteredQueryGetAll()
        {
            return Repository;
        }
        #endregion

        #region Mapper
        protected virtual TViewOutput MapToViewOutput(TEntity entity)
        {
            return ObjectMapper.Map<TEntity, TViewOutput>(entity);
        }

        protected virtual TEditOutput MapToEditOutput(TEntity input)
        {
            return ObjectMapper.Map<TEntity, TEditOutput>(input);
        }

        protected virtual TEntity MapToEntity(TCreateUpdate input)
        {
            return ObjectMapper.Map<TCreateUpdate, TEntity>(input);
        }
        protected virtual void MapToEntity(TCreateUpdate createOrEditInput, TEntity entity)
        {
            ObjectMapper.Map<TCreateUpdate, TEntity>(createOrEditInput, entity);
        }
        #endregion

        #region Sorting and Paging
        protected virtual IQueryable<TEntity> ApplySorting(IQueryable<TEntity> query, TGetAllInput input)
        {
            if (input is ISortedResultRequest sortInput)
            {
                if (!sortInput.Sorting.IsNullOrWhiteSpace())
                {
                    return query.OrderBy(sortInput.Sorting);
                }
            }
            return query.OrderByDescending(x => x.Id);
        }

        protected virtual IQueryable<TEntity> ApplyPaging(IQueryable<TEntity> query, TGetAllInput input)
        {
            if (input is IPagedResultRequest pagedResultRequest)
            {
                return query.PageBy(pagedResultRequest);
            }
            return query;
        }
        #endregion

        #region create and update

        protected virtual async Task<TEntity> Create(TCreateUpdate input)
        {

            var entity = MapToEntity(input);

            await Repository.InsertAsync(entity);

            await CurrentUnitOfWork.SaveChangesAsync();

            return entity;
        }
        protected virtual async Task<TEntity> Update(TCreateUpdate input)
        {

            var entity = await Repository.GetAsync(input.Id.Value);

            if (entity == null) throw new UserFriendlyException("Đối tượng không tồn tại");

            MapToEntity(input, entity);

            await CurrentUnitOfWork.SaveChangesAsync();

            return entity;
        }

        #endregion

        #region public method

        public virtual async Task Delete(TPrimaryKey id)
        {
            await Repository.DeleteAsync(id);
        }

        public virtual async Task<TEditOutput> Get(TPrimaryKey id)
        {

            var result = await Repository.GetAsync(id);

            if (result == null) throw new UserFriendlyException("Đối tượng không tồn ");

            return MapToEditOutput(result);
        }

        public virtual async Task<List<TViewOutput>> GetAll()
        {

            var query = CreateFilteredQueryGetAll();
            var result = await AsyncExecuter.ToListAsync(query);

            return result.Select(MapToViewOutput).ToList();
        }

        public virtual async Task<PagedResultDto<TViewOutput>> GetListPaged(TGetAllInput input)
        {

            var query = CreateFilteredQuery(input);

            var totalCount = await AsyncExecuter.CountAsync(query);

            query = ApplySorting(query, input);
            query = ApplyPaging(query, input);

            var result = await AsyncExecuter.ToListAsync(query);

            return new PagedResultDto<TViewOutput>(totalCount, result.Select(MapToViewOutput).ToList());
        }

        public virtual async Task<TPrimaryKey> Save(TCreateUpdate input)
        {
            TEntity entity;
            if (input.Id == null || $"{input.Id}" == "0")
            {
                entity = await Create(input);
            }
            else
            {
                entity = await Update(input);
            }

            return entity.Id;
        }

        #endregion
    }
}

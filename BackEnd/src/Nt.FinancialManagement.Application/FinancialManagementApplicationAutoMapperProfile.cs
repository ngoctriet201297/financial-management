﻿using AutoMapper;

namespace Nt.FinancialManagement
{
    public class FinancialManagementApplicationAutoMapperProfile : Profile
    {
        public FinancialManagementApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
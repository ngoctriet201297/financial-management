﻿using Nt.FinancialManagement.Localization;
using Volo.Abp.Application.Services;

namespace Nt.FinancialManagement
{
    public abstract class FinancialManagementAppService : ApplicationService
    {
        protected FinancialManagementAppService()
        {
            LocalizationResource = typeof(FinancialManagementResource);
            ObjectMapperContext = typeof(FinancialManagementApplicationModule);
        }
    }
}

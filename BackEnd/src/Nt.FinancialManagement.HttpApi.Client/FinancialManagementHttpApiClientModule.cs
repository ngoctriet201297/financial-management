﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace Nt.FinancialManagement
{
    [DependsOn(
        typeof(FinancialManagementApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class FinancialManagementHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "FinancialManagement";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(FinancialManagementApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}

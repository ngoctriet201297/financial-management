﻿using System;
using Microsoft.EntityFrameworkCore;
using Nt.FinancialManagement.Entities;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Nt.FinancialManagement.EntityFrameworkCore
{
    public static class FinancialManagementDbContextModelCreatingExtensions
    {
        public static void ConfigureFinancialManagement(
            this ModelBuilder builder,
            Action<FinancialManagementModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new FinancialManagementModelBuilderConfigurationOptions(
                FinancialManagementDbProperties.DbTablePrefix,
                FinancialManagementDbProperties.DbSchema
            );

            optionsAction?.Invoke(options);

            /* Configure all entities here. Example:

            builder.Entity<Question>(b =>
            {
                //Configure table & schema name
                b.ToTable(options.TablePrefix + "Questions", options.Schema);
            
                b.ConfigureByConvention();
            
                //Properties
                b.Property(q => q.Title).IsRequired().HasMaxLength(QuestionConsts.MaxTitleLength);
                
                //Relations
                b.HasMany(question => question.Tags).WithOne().HasForeignKey(qt => qt.QuestionId);

                //Indexes
                b.HasIndex(q => q.CreationTime);
            });
            */

            builder.Entity<FileEntity>(b =>
            {
                //Configure table & schema name
                b.ToTable(options.TablePrefix + "FileEntities", options.Schema);

                b.ConfigureByConvention();
            });

            builder.Entity<Payment>(b =>
            {
                //Configure table & schema name
                b.ToTable(options.TablePrefix + "Payments", options.Schema);

                b.ConfigureByConvention();
            });

            builder.Entity<Receipt>(b =>
            {
                //Configure table & schema name
                b.ToTable(options.TablePrefix + "Receipts", options.Schema);

                b.ConfigureByConvention();
            });
        }
    }
}
﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Nt.FinancialManagement.EntityFrameworkCore
{
    public class FinancialManagementModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
    {
        public FinancialManagementModelBuilderConfigurationOptions(
            [NotNull] string tablePrefix = "",
            [CanBeNull] string schema = null)
            : base(
                tablePrefix,
                schema)
        {

        }
    }
}
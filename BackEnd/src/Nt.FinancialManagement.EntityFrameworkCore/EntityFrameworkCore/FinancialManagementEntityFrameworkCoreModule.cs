﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore.MySQL;
using Volo.Abp.Modularity;

namespace Nt.FinancialManagement.EntityFrameworkCore
{
    [DependsOn(
        typeof(FinancialManagementDomainModule),
        typeof(AbpEntityFrameworkCoreMySQLModule)
    )]
    public class FinancialManagementEntityFrameworkCoreModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<FinancialManagementDbContext>(options =>
            {
                /* Add custom repositories here. Example:
                 * options.AddRepository<Question, EfCoreQuestionRepository>();
                 */
            });
        }
    }
}
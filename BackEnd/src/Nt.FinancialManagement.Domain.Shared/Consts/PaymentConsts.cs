﻿using System;
namespace Nt.FinancialManagement.Consts
{
    public class PaymentConsts
    {
        public const int MaxLengthDescription = 255;
        public const int MaxLengthAddress = 255;
        public const int MaxLengthPhoneNumber = 50;
    }
}

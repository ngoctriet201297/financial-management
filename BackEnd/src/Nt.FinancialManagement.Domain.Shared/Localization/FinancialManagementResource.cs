﻿using Volo.Abp.Localization;

namespace Nt.FinancialManagement.Localization
{
    [LocalizationResourceName("FinancialManagement")]
    public class FinancialManagementResource
    {
        
    }
}

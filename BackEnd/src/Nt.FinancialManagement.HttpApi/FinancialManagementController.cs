﻿using Nt.FinancialManagement.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Nt.FinancialManagement
{
    public abstract class FinancialManagementController : AbpController
    {
        protected FinancialManagementController()
        {
            LocalizationResource = typeof(FinancialManagementResource);
        }
    }
}

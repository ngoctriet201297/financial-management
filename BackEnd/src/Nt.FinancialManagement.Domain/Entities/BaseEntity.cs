﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;

namespace Nt.FinancialManagement.Entities
{
    public class BaseEntity : FullAuditedEntity<Guid>
    {

    }
}

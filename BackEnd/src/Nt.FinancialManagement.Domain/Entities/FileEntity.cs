﻿using System;
using System.ComponentModel.DataAnnotations;
using Nt.FinancialManagement.Consts;

namespace Nt.FinancialManagement.Entities
{
    public class FileEntity : BaseEntity
    {
        [MaxLength(FileEntityConsts.MaxLengthName)]
        public string Name { get; set; }
    }
}

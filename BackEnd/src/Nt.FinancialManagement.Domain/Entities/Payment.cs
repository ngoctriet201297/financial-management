﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Nt.FinancialManagement.Consts;

namespace Nt.FinancialManagement.Entities
{
    public class Payment : BaseEntity
    {
        [MaxLength(PaymentConsts.MaxLengthDescription)]
        public string Description { get; set; }
        [MaxLength(PaymentConsts.MaxLengthAddress)]
        public string Address { get; set; }
        [MaxLength(PaymentConsts.MaxLengthPhoneNumber)]
        public string PhoneNumber { get; set; }
        public double Amount { get; set; }
        public Guid? PictureId { get; set; }
        [ForeignKey("PictureId")]
        public FileEntity Picture { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Nt.FinancialManagement.Consts;

namespace Nt.FinancialManagement.Entities
{
    public class Receipt : BaseEntity
    {
        [MaxLength(ReceiptConsts.MaxLengthDescription)]
        public string Description { get; set; }
        [MaxLength(ReceiptConsts.MaxLengthAddress)]
        public string Address { get; set; }
        [MaxLength(ReceiptConsts.MaxLengthPhoneNumber)]
        public string PhoneNumber { get; set; }
        public double Amount { get; set; }
        public Guid? PictureId { get; set; }
        [ForeignKey("PictureId")]
        public FileEntity Picture { get; set; }
    }
}

﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace Nt.FinancialManagement
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(FinancialManagementDomainSharedModule)
    )]
    public class FinancialManagementDomainModule : AbpModule
    {

    }
}

﻿using Volo.Abp.Settings;

namespace Nt.FinancialManagement.Settings
{
    public class FinancialManagementSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from FinancialManagementSettings class.
             */
        }
    }
}
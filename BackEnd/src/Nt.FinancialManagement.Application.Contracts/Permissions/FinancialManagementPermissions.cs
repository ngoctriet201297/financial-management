﻿using Volo.Abp.Reflection;

namespace Nt.FinancialManagement.Permissions
{
    public class FinancialManagementPermissions
    {
        public const string GroupName = "FinancialManagement";

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(FinancialManagementPermissions));
        }
    }
}
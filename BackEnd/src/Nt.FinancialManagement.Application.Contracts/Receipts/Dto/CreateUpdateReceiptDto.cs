﻿using System;
using System.ComponentModel.DataAnnotations;
using Nt.FinancialManagement.Consts;

namespace Nt.FinancialManagement.Receipts.Dto
{
    public class CreateUpdateReceiptDto : BaseEntityDto<Guid?>
    {
        [MaxLength(ReceiptConsts.MaxLengthDescription)]
        public string Description { get; set; }
        [MaxLength(ReceiptConsts.MaxLengthAddress)]
        public string Address { get; set; }
        [MaxLength(ReceiptConsts.MaxLengthPhoneNumber)]
        public string PhoneNumber { get; set; }
        public double Amount { get; set; }
        public Guid? PictureId { get; set; }
    }
}

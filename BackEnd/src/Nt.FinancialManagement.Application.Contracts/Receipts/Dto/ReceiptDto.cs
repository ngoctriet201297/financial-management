﻿using System;
namespace Nt.FinancialManagement.Receipts.Dto
{
    public class ReceiptDto : BaseEntityDto
    {
        public string Description { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public double Amount { get; set; }
        public Guid? PictureId { get; set; }
    }
}

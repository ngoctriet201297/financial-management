﻿using System;
using Nt.FinancialManagement.Receipts.Dto;

namespace Nt.FinancialManagement.Receipts
{
    public interface IReceiptAppService : IAppBaseService<Guid, CreateUpdateReceiptDto, GetReceiptForViewDto, GetReceiptForEditDto, GetReceiptInputDto>
    {
        
    }
}

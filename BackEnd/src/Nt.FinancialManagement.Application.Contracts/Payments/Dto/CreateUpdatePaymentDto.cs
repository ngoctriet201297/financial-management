﻿using System;
using System.ComponentModel.DataAnnotations;
using Nt.FinancialManagement.Consts;

namespace Nt.FinancialManagement.Payments.Dto
{
    public class CreateUpdatePaymentDto : BaseEntityDto<Guid?>
    {
        [MaxLength(PaymentConsts.MaxLengthDescription)]
        public string Description { get; set; }
        [MaxLength(PaymentConsts.MaxLengthAddress)]
        public string Address { get; set; }
        [MaxLength(PaymentConsts.MaxLengthPhoneNumber)]
        public string PhoneNumber { get; set; }
        public double Amount { get; set; }
        public Guid? PictureId { get; set; }
    }
}

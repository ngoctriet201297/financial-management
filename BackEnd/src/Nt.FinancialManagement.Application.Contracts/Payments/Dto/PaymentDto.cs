﻿using System;
namespace Nt.FinancialManagement.Payments.Dto
{
    public class PaymentDto : BaseEntityDto
    {
        public string Description { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public double Amount { get; set; }
        public Guid? PictureId { get; set; }
    }
}

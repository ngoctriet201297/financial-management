﻿using System;
using Nt.FinancialManagement.Payments.Dto;

namespace Nt.FinancialManagement.Payments
{
    public interface IPaymentAppService : IAppBaseService<Guid, CreateUpdatePaymentDto, GetPaymentForViewDto, GetPaymentForEditDto, GetPaymentInputDto >
    {
        
    }
}

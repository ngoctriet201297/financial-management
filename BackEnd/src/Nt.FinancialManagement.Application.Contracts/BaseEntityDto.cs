﻿using System;
using Volo.Abp.Application.Dtos;

namespace Nt.FinancialManagement
{
    public class BaseEntityDto<TKey> : FullAuditedEntityDto<TKey>
    {
        
    }
    public class BaseEntityDto : FullAuditedEntityDto<Guid>
    {

    }
}

﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Nt.FinancialManagement.EntityFrameworkCore
{
    public class FinancialManagementHttpApiHostMigrationsDbContextFactory : IDesignTimeDbContextFactory<FinancialManagementHttpApiHostMigrationsDbContext>
    {
        public FinancialManagementHttpApiHostMigrationsDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<FinancialManagementHttpApiHostMigrationsDbContext>()
                .UseMySql(configuration.GetConnectionString("FinancialManagement"), ServerVersion.FromString("8.0.21-mysql"));

            return new FinancialManagementHttpApiHostMigrationsDbContext(builder.Options);
        }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EcoWorld.Organization.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Nt.FinancialManagement.Entities;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Domain.Repositories;

namespace Nt.FinancialManagement.Controllers
{
    public class FileAppController : AbpController
    {
        private readonly IRepository<FileEntity> _fileRepository;
        private readonly string _resourcePath;

        public FileAppController(IRepository<FileEntity> fileRepository,
                                 IConfiguration configuration)
        {
            _fileRepository = fileRepository;
            _resourcePath = Path.Combine(Directory.GetCurrentDirectory(), configuration["Resources"]);
        }

        public async Task<List<Guid>> Upload()
        {

            var files = HttpContext?.Request.Form.Files;
            var newFilesId = new List<Guid>();

            if (files?.Count > 0)
            {
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        var fileEntity = new FileEntity();
                        await _fileRepository.InsertAsync(fileEntity);
                        await CurrentUnitOfWork.SaveChangesAsync();
                        var fileExtension =
                            Path.GetExtension(ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName
                                ?.Trim())?.Replace("\"", "");
                        var fileName = fileEntity.Id + fileExtension;
                        var fullPath = CreateFullPath(fileEntity.CreationTime, fileName);

                        fileEntity.Name = fileName;

                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }

                        newFilesId.Add(fileEntity.Id);
                        await CurrentUnitOfWork.SaveChangesAsync();
                    }
                }
            }

            return newFilesId;
        }
        public async Task Delete(Guid id)
        {
            await _fileRepository.DeleteAsync(x => x.Id == id);
        }
        public async Task<object> Get(Guid id, int? width, int? height)
        {
            var fileEntity = await _fileRepository.GetAsync(x=> x.Id == id);
            if (fileEntity != null)
            {
                string path = GetPath(fileEntity.CreationTime, fileEntity.Name);
                ;

                var fileExtension = Path.GetExtension(path);

                if (width.HasValue && height.HasValue && FileHelper.CheckFileIsImage(path))
                {
                    var requestedFileName = Path.GetFileNameWithoutExtension(fileEntity.Name) +
                                            $"width{width}height{height}{fileExtension}";
                    string newPath = GetPath(fileEntity.CreationTime, requestedFileName);
                    await FileHelper.SaveImageWithNewSize(path, height.Value, width.Value, newPath);
                    path = newPath;
                }

                FileStream stream = new FileStream(path, FileMode.Open);
                FileStreamResult result = new FileStreamResult(stream, FileHelper.GetMimeType(fileExtension));
                return result;
            }

            return null;
        }



        private string CreateFullPath(DateTime date, string fileName)
        {

            var fullPath = Path.Combine(_resourcePath, $"{date.Year}/{date.Month}/{date.Day}");
            Directory.CreateDirectory(fullPath);
            return fullPath + $"/{fileName}";
        }

        private string GetPath(DateTime date, string fileName)
        {
            return Path.Combine(_resourcePath, $"{date.Year}/{date.Month}/{date.Day}/{fileName}");
        }
    }
}
